#include <dlfcn.h>
#include <type_traits>
#include <cstring>
#include "backend.hpp"
#include "exception.hpp"

namespace Nov {
namespace Database {

Backend::Backend(const std::string& backendName) {
	const std::string path = "libnov_database_" + backendName + ".so." NOV_VERSION_SO;
	handle = dlopen(path.c_str(), RTLD_LAZY);
	if (!handle) {
		throw Exception("Error loading " + backendName + " backend: " + dlerror());
	}
}

Backend::~Backend() {
	dlclose(handle);
}

template<typename T>
static T voidToFunc(void * p) {
	static_assert(std::is_pointer<T>::value, "Not a pointer");
	T tmp = nullptr;
	std::memcpy(&tmp, &p, sizeof p);
	return tmp;
}

std::shared_ptr<Configuration> Backend::createConfiguration(const std::string& connectString) {
	using CreateFunc = std::shared_ptr<Configuration>(const std::string&);
	using CreateFuncPtr = CreateFunc*;

	CreateFuncPtr func = voidToFunc<CreateFuncPtr>( dlsym(handle, "newConfiguration") );
	if (!func) {
		throw Exception(std::string() + "Error loading newConfiguration function: " + dlerror());
	}
	return func(connectString);
}

} // namespace Database
} // namespace Nov
