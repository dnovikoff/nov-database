#ifndef NOV_DATABASE_TRANSACTION_HPP_
#define NOV_DATABASE_TRANSACTION_HPP_

#include <memory>

namespace Nov {
namespace Database {

class Connection;

class TransactionScope final {
public:
	explicit TransactionScope(const std::shared_ptr<Connection>& c);
	~TransactionScope();

	TransactionScope(TransactionScope&&) = default;
	TransactionScope& operator=(TransactionScope&&) = default;

	// Use commit before the destructor
	void forceCommit();
	// Use commit before the destructor
	void forceRollback();
private:
	void validateConnection();
	std::shared_ptr<Connection> connection;

	TransactionScope(const TransactionScope&) = delete;
	TransactionScope& operator=(const TransactionScope&) = delete;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TRANSACTION_HPP_
