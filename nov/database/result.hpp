#ifndef NOV_DATABASE_RESULT_HPP_
#define NOV_DATABASE_RESULT_HPP_

#include <memory>
#include <vector>

#include <nov/database/param.hpp>

namespace Nov {
namespace Database {

class Statement;

// Result is Statement adapter, that allows only fetch interface
// This class is ok to copy
// Note that the result will become invalid if statement reexecuted

class Result final {
public:
	bool fetch(OutputParams& output);
	size_t getAffacted() const { return affectedRows; }
	Result(Result&&) = default;
	Result& operator=(Result&&) = default;

	Result();
private:
	friend class Statement;

	std::shared_ptr<Statement> statement;
	size_t affectedRows = 0;

	Result(const Result&) = delete;
	Result& operator=(const Result&) = delete;
};

class BindedResult {
public:
	bool fetch();
	size_t getAffacted() const { return result.getAffacted(); }

	BindedResult(BindedResult&&) = default;
	BindedResult& operator=(BindedResult&&) = default;
private:
	friend class Stream;
	BindedResult();

	Result result;
	std::shared_ptr<OutputParams> outputParams;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_RESULT_HPP_
