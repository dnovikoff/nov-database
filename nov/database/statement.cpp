#include "statement.hpp"
#include "exception.hpp"
#include "param.hpp"
#include "logger.hpp"
#include "result.hpp"
#include "param_visitor.hpp"

namespace Nov {
namespace Database {

namespace {
using namespace Types;

class ParamPrintVisitor: public ParamReadVisitor {
	std::ostringstream out;
	bool first = true;

	template<typename T>
	void print(const std::string& name, const T* value) {
		if (!first) {
			out << ", ";
		} else {
			first = false;
		}
		out << "<" << name << " ";
		if (value == nullptr) {
			out << "NULL";
		} else {
			out << *value;
		}
		out << ">";
	}

public:
	void operator()(const String* value) override {
		print("String", value);
	}
	void operator()(const Integer* value) override {
		print("Integer", value);
	}
	void operator()(const Double* value) override {
		print("Double", value);
	}
	void operator()(const Timestamp* value) override {
		print("Timestamp", value);
	}
	void operator()(const Binary* value) override {
		print("Binary", value);
	}
	std::string toString() const {
		return out.str();
	}
};
} // namespace

Statement::Statement(const std::string& q):query(q) {}

Result Statement::execute(const InputParams& input) {
	if (state == INITIALIZED) {
		logger()->debug() << "[" << this<< " Prepare] " << query;
		doPrepare(query);
		state = PREPARED;
	}
	if (logger()->checkLevel(Log::LogLevel::DEBUG)) {
		if (input.empty()) {
			logger()->debug() << "[" << this << " Execute] No input data";
		} else {
			ParamPrintVisitor visitor;
			visitor.visitCollection(input);
			logger()->debug() << "[" << this << " Execute] " << visitor.toString();
		}
	}
	auto r = doExecute(input);
	if (r > 0 && logger()->checkLevel(Log::LogLevel::DEBUG)) {
		logger()->debug() << "[" << this << " ExecuteResult] " << r << " row(s) affected";
	}
	state = EXECUTED;
	lastFetchResult = true;

	Result res;
	res.statement = shared_from_this();
	res.affectedRows = r;
	return res;
}

Result Statement::execute() {
	static const InputParams emptyParams;
	return execute(emptyParams);
}

bool Statement::fetch(OutputParams& output) {
	if (!lastFetchResult) {
		return false;
	}
	if (state != EXECUTED) {
		logger()->error() << "[" << this << " fetch] Statement not prepared";
		throw Exception("Statement not prepared");
	}
	lastFetchResult = doFetch(output);

	if (lastFetchResult) {
		if (logger()->checkLevel(Log::LogLevel::DEBUG)) {
			ParamPrintVisitor visitor;
			visitor.visitCollection(output);
			logger()->debug() << "[" << this << " fetch] " << visitor.toString();
		}
	} else {
		logger()->debug() << "[" << this << " fetch] End";
	}
	return lastFetchResult;
}

Statement::~Statement() {}

} // namespace Database
} // namespace Nov
