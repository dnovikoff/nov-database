#include "logger.hpp"

#include <cassert>

namespace Nov {
namespace Database {

static Nov::Log::Logger* loggerPtr = nullptr;

void setLogger(Nov::Log::Logger* logger) {
	loggerPtr = logger;
}

Nov::Log::Logger* logger() {
	assert(loggerPtr);
	return loggerPtr;
}

} // namespace Database
} // namespace Nov
