#ifndef NOV_DATABASE_CONFIGURATION_HPP_
#define NOV_DATABASE_CONFIGURATION_HPP_

#include <memory>

namespace Nov {
namespace Database {

class Connection;

class Configuration {
	Configuration(const Configuration&) = delete;
	Configuration& operator=(const Configuration&) = delete;
public:
	Configuration() {}
	virtual std::shared_ptr<Connection> newConnection() = 0;
	virtual ~Configuration() {}
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_CONFIGURATION_HPP_
