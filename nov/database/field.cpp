#include "field.hpp"

namespace Nov {
namespace Database {

Field::Field(const std::string& name):fieldName(name) {
}

const BindedField Field::bind(const ParamRef& param) const {
	return BindedField(*this, param);
}

const AssignedField Field::assignCopy(const ParamCopy& param) const {
	return AssignedField(*this, param);
}

const AssignedField Field::assignRef(const ParamRef& param) const {
	return AssignedField(*this, param);
}

const AssignedField Field::assign(const Param& param) const {
	return AssignedField(*this, param);
}

BindedField::BindedField(const Field& f, const ParamRef& p):field(f), param(p) {
}

void BindedField::visitParamForRead(ParamReadVisitor& v) const {
	param.visitForRead(v);
}

AssignedField::AssignedField(const Field& f, const Param& p):field(f), param(p) {

}
void AssignedField::visitParamForRead(ParamReadVisitor& v) const {
	param.visitForRead(v);
}

AssignList::AssignList(const std::string& sep, const AssignedFields& d):separator(sep), data(d) {
}

} // namespace Database
} // namespace Nov
