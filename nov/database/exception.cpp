#include "exception.hpp"
#include "logger.hpp"

namespace Nov {
namespace Database {

void throwException(const std::string& message) {
	logger()->error() << message;
	throw Exception(message);
}

} // namespace Database
} // namespace Nov
