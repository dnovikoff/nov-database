#include <memory>

#include <nov/database/statement.hpp>
#include <nov/database/result.hpp>

#include "connection.hpp"
#include "stream.hpp"
#include "stream_imp.hpp"
#include "meta.hpp"
#include "param.hpp"

namespace Nov {
namespace Database {

Result Connection::execute(const std::string& query) {
	auto st = newStatement(query);
	return st->execute();
}

Connection::Connection() {
}

Connection::~Connection() {
}

Stream Connection::createStream() {
	Stream s(newStreamImp());
	s.imp->connection = shared_from_this();
	return s;
}

class StreamImp* Connection::newStreamImp() {
	return new StreamImp;
}

void Connection::insert(const Meta& meta, const AssignedFields& assigned, ParamRef returnId) {
	Stream q = createStream();
	Fields fields;
	InputParams params;

	fields.reserve(assigned.size());
	params.reserve(assigned.size());

	for (auto& f : assigned) {
		fields.push_back(f.getField());
		params.push_back(f.getParam());
	}

	q << "INSERT INTO " << meta.getTable() << " (" << fields << ") VALUES (" << params << ")";
	if (meta.getSerial().name().empty()) {
		q.execute();
		return;
	}
	executeInsertAndReturnId(q, meta, returnId);
}

void Connection::insert(const Meta& meta, const AssignedFields& params) {
	Types::Integer i = 0;
	ParamRef r(i);
	insert(meta, params, r);
}

bool Connection::executeUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded) {
	auto result = doExecuteUpdateReturning(updateQuery, meta, binded);
	return result.fetch();
}

void Connection::startTransaction() {
	execute(startTransactionCommand());
}

void Connection::commitTransaction() {
	execute(commitTransactionCommand());
}

void Connection::rollbackTransaction() {
	execute(rollbackTransactionCommand());
}

} // namespace Database
} // namespace Nov
