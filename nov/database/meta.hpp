#ifndef NOV_DATABASE_META_HPP_
#define NOV_DATABASE_META_HPP_

#include <vector>
#include <string>
#include <type_traits>

#include <nov/database/field.hpp>
#include <nov/database/param.hpp>
#include <nov/database/key.hpp>

namespace Nov {
namespace Database {

template<typename T>
class ZeroValue {
	ZeroValue() = delete;
public:
	static T value() {
		T value;
		return value;
	}
};

// Base class for all meta-classes
class Meta {
	friend class MetaHolder;

	template<typename T>
	class PodZeroValue {
		PodZeroValue() = delete;
	public:
		static T value() {
			return 0;
		}
	};

	template<typename T, bool isPod = std::is_pod<T>::value  && !std::is_enum<T>::value>
	struct ZeroSelect {
		typedef ZeroValue<T> type;
	};

	template<typename T>
	struct ZeroSelect<T, true> {
		typedef PodZeroValue<T> type;
	};

	template<typename T>
	static T zeroValue() {
		return ZeroSelect<T>::type::value();
	}

public:
	virtual ~Meta() = 0;

	struct ForeignDescr {
		Key key;
		const Meta* meta;
		ForeignDescr(const Key& k, const Meta* const m):key(k), meta(m) {
		}
	};

	typedef std::vector<Key> Keys;
	typedef std::vector<ForeignDescr> ForeignKeys;

	const Field& getTable() const {
		return table;
	}
	const Field& getSerial() const {
		return serial;
	}
	const AssignedFields& getAssignedFields() const {
		return assignedFields;
	}
	const Fields& getFields() const {
		return fields;
	}
	const Keys& getKeys() const {
		return keys;
	}
	const ForeignKeys& getForeignKeys() const {
		return foreignKeys;
	}
	const Key& getPrimaryKey() const;

	void initKeysIfNotInited() {
		if (keysInitialized) {
			return;
		}
		initIfNotInited();
		initKeys();
		keysInitialized = true;
	}

	void initIfNotInited() {
		if (initialized) {
			return;
		}
		init();
		initialized = true;
	}

protected:
	explicit Meta(const std::string& tableName);

	template<typename FieldType>
	void addField(const Field& field) {
		ParamCopy param(zeroValue<FieldType>());
		addField(field, param);
	}
	void setSerial(const Field& field);
	void addKey(const Key& key);
	void addForeignKey(const Key& linkFrom, const Meta& linkTo);

	virtual void init() = 0;
	virtual void initKeys() = 0;

	Meta(Meta&&) = default;

private:
	void addField(const Field& field, const Param& p);
	void addField(const AssignedField& fieldInfo);

	Meta(const Meta&) = delete;
	Meta& operator=(const Meta&) = delete;

	Field table;
	Fields fields;
	AssignedFields assignedFields;
	Keys keys;
	ForeignKeys foreignKeys;
	Field serial;
	Key* primaryKey = nullptr;
	bool keysInitialized = false;
	bool initialized = false;
};

} // namespace Database
} // namespace Nov

#define NOV_DB_OBJECT(X, NAME) const Nov::Database::Field X = Nov::Database::Field(NAME)
#define NOV_DB_FIELD(X) NOV_DB_OBJECT(X, #X)

#endif // NOV_DATABASE_META_HPP_
