#ifndef NOV_DATABASE_KEY_HPP_
#define NOV_DATABASE_KEY_HPP_

#include <string>
#include <vector>

#include <nov/database/field.hpp>

namespace Nov {
namespace Database {

class Key {
public:
	typedef std::vector<Field> Fields;
	enum Type { Primary=1, Unique, Index };

	void setType(Type t) {
		type = t;
	}
	void addField(const Field& field) {
		fields.push_back(field);
	}
	Type getType() const {
		return type;
	}
	const Fields& getFields() const {
		return fields;
	}
	virtual ~Key() {}
protected:
	explicit Key(Type t):type(t) {}
private:
	Type type;
	Fields fields;
};

class PrimaryKey: public Key {
public:
	PrimaryKey():Key(Primary) {}
};

class UniqueKey: public Key {
public:
	UniqueKey():Key(Unique) {}
};

class IndexKey: public Key {
public:
	IndexKey():Key(Index) {}
};

template<typename StreamType>
inline void writeKeyFields(StreamType& query, const Key& key) {
	const auto& fields = key.getFields();
	bool first = true;
	for (const auto& field: fields) {
		if(first) {
			first = false;
		} else {
			query << ", ";
		}
		query << field;
	}
}

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_KEY_HPP_
