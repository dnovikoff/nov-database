#ifndef NOV_DATABASE_META_HOLDER_HPP_
#define NOV_DATABASE_META_HOLDER_HPP_

#include <memory>
#include <vector>
#include <nov/database/meta.hpp>

namespace Nov {
namespace Database {

class MetaHolder {
public:
	typedef std::function<void(Meta&)> Callback;

	MetaHolder();
	~MetaHolder();

	MetaHolder(MetaHolder&&);
	MetaHolder& operator=(MetaHolder&&);

	void init();
	void initKeys();
	void apply(const Callback& func);

	void add(std::unique_ptr<Meta> meta);

	template<typename T>
	void add() {
		auto p = new T;
		add(std::unique_ptr<Meta>(p));
		T::setInstance(p);
	}

private:
	std::vector<std::unique_ptr<Meta> > metas_;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_META_HOLDER_HPP_
