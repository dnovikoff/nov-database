#ifndef NOV_DATABASE_LOGGER_HPP_
#define NOV_DATABASE_LOGGER_HPP_

#include <nov/log/logger.hpp>

namespace Nov {
namespace Database {

extern Nov::Log::Logger* logger();
extern void setLogger(Nov::Log::Logger* logger);

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_LOGGER_HPP_
