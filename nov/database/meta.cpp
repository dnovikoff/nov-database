#include "meta.hpp"

namespace Nov {
namespace Database {

Meta::Meta(const std::string& tableName):table(tableName), serial("") {
	fields.reserve(32);
	assignedFields.reserve(32);
	keys.reserve(32);
}

Meta::~Meta() {
}

void Meta::addField(const AssignedField& fieldInfo) {
	fields.push_back(fieldInfo.getField());
	assignedFields.push_back(fieldInfo);
}

void Meta::addField(const Field& field, const Param& p) {
	addField(field.assign(p));
}

void Meta::setSerial(const Field& field) {
	serial = field;
}

void Meta::addKey(const Key& key) {
	keys.push_back(key);
	if (key.getType() == Key::Primary) {
		if (primaryKey) {
			throw std::logic_error("Meta " + getTable().name() + ": multiple Primary Keys not allowed");
		}
		primaryKey = &keys.back();
	}
}

void Meta::addForeignKey(const Key& linkFrom, const Meta& linkTo) {
	foreignKeys.push_back( ForeignDescr(linkFrom, &linkTo) );
}

const Key& Meta::getPrimaryKey() const {
	if (!primaryKey) {
		throw std::logic_error("Meta " + getTable().name() + " does not have Primary Key");
	}
	return *primaryKey;
}

} // namespace Database
} // namespace Nov
