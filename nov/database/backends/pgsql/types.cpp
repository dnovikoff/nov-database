#define PG_PORT_H // Do not includ one header

#include <postgres_fe.h>
#include <catalog/pg_type.h>

#include "types.hpp"

namespace Nov {
namespace Database {
namespace PgSql {

const Oid PgTypes::Int16    = INT2OID;
const Oid PgTypes::Int32    = INT4OID;
const Oid PgTypes::Int64    = INT8OID;
const Oid PgTypes::Numeric  = NUMERICOID;
const Oid PgTypes::Double   = FLOAT8OID;
const Oid PgTypes::Varchar  = VARCHAROID;
const Oid PgTypes::Char     = BPCHAROID;
const Oid PgTypes::String   = TEXTOID;
const Oid PgTypes::Binary   = BYTEAOID;
const Oid PgTypes::DateTime = TIMESTAMPTZOID;

const Oid PgTypes::Unknown  = UNKNOWNOID;

const char* PgTypes::name(Oid type) {
#define TMP( X ) if( type == X ) return #X;
	// TODO: switch
	TMP( Int16    );
	TMP( Int32    );
	TMP( Int64    );
	TMP( Numeric  );
	TMP( Double   );
	TMP( Varchar  );
	TMP( Char     );
	TMP( String   );
	TMP( Binary   );
	TMP( DateTime );

	TMP( Unknown  );
#undef TMP
	return "Not registed type";
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
