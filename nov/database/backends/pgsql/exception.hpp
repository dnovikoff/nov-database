#ifndef NOV_DATABASE_BACKENDS_PGSQL_EXCEPTION_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_EXCEPTION_HPP_

#include <string>

namespace Nov {
namespace Database {
namespace PgSql {

extern void throwException(const std::string&);

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_EXCEPTION_HPP_
