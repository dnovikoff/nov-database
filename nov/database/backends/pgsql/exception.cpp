#include "exception.hpp"

#include <nov/database/exception.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

void throwException(const std::string& message) {
	const std::string newMessage = "pgsql error: " + message;
	Database::throwException(newMessage);
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
