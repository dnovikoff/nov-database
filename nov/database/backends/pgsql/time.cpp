#include "time.hpp"

namespace Nov {
namespace Database {
namespace PgSql {

static const Types::Timestamp date2000(boost::gregorian::date(2000,1,1));

int64_t timestampToPgTime(const Types::Timestamp& x) {
	const int64_t mss = (x - date2000).total_microseconds();
	return mss;
}

Types::Timestamp pgTimeToTimestamp(int64_t x) {
	// Some bug using microseconds on debug - so using milliseconds
	// It seems fixed in boost 53
	const auto ms = boost::posix_time::milliseconds(x/(1000));
	Types::Timestamp ts(date2000 + ms);
	return ts;
}

} // namespace PgSql
} // namespace Database
} // namespace Nov

