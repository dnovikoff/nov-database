#include "configuration.hpp"
#include "connection.hpp"

namespace Nov {
namespace Database {
namespace PgSql {

ConfigurationImp::ConfigurationImp(const std::string& info):connectionInfo(info) {
}

std::shared_ptr<Connection> ConfigurationImp::newConnection() {
	return std::make_shared<ConnectionImp>(connectionInfo);
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
