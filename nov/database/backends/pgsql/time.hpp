#ifndef NOV_DATABASE_BACKENDS_PGSQL_TIME_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_TIME_HPP_

#include <stdint.h>
#include <nov/database/types/timestamp.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

int64_t timestampToPgTime(const Types::Timestamp&);
Types::Timestamp pgTimeToTimestamp(int64_t);

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_TIME_HPP_
