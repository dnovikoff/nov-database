#include <memory>
#include <string>

#include "configuration.hpp"

extern "C" std::shared_ptr<Nov::Database::Configuration> newConfiguration(const std::string& connectString) {
	return std::make_shared<Nov::Database::PgSql::ConfigurationImp>(connectString);
}
