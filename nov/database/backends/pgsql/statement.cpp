#include <boost/lexical_cast.hpp>

#include "statement.hpp"
#include "exception.hpp"
#include "input.hpp"
#include "column.hpp"
#include <nov/database/param.hpp>

#include <libpq-fe.h>

namespace Nov {
namespace Database {
namespace PgSql {

static inline PGconn* _pgConn(void* d) {
	return reinterpret_cast<PGconn*> ( d );
}

static inline PGresult* _pgResult(void* d) {
	return reinterpret_cast<PGresult*> ( d );
}

#define pgConnection _pgConn(connection)
#define pgResult _pgResult(result)

StatementImp::StatementImp(const std::string& query, void* con):Statement(query), connection(con) {}

StatementImp::~StatementImp() {
	close();
}

void StatementImp::close() {
	if (result != nullptr) {
		PQclear(pgResult);
		result = nullptr;
	}
}

void StatementImp::doPrepare(const std::string& q) {
	query = q;
	// Real prepare moved to execute
}

StatementImp::ResultType StatementImp::getResultType() const {
	if ( result == nullptr) return FatalError;
	const ExecStatusType status = PQresultStatus(pgResult);
	if(status == PGRES_COMMAND_OK) return NoData;
	if(status == PGRES_TUPLES_OK ) return Data;
	return Error;
}

const char * StatementImp::getDataAt(int c) const {
	return PQgetvalue( pgResult, iterator, c);
}

namespace {
using namespace Nov::Database::Types;

class InputVisitor: public ParamReadVisitor {
	Input input;

	template<typename T>
	void addInput(const T& v) {
		input.add(v);
	}
	template<typename T>
	void add(const T* const v) {
		if (v == nullptr) {
			input.addNull<T>();
			return;
		}
		addInput(*v);
	}
public:
	void operator()(const Types::String* v) override {
		add(v);
	}
	void operator()(const Types::Integer* v) override {
		add(v);
	}
	void operator()(const Types::Double* v) override {
		add(v);
	}
	void operator()(const Types::Timestamp* v) override {
		add(v);
	}
	void operator()(const Types::Binary* v) override {
		if (v == nullptr) {
			input.addNull<Types::BinaryData>();
			return;
		}
		input.add(v->encode());
	}

	const Input& getInput() const {
		return input;
	}
};

} // namespace

size_t StatementImp::doExecute(const InputParams& input) {
	close();
	InputVisitor visitor;
	visitor.visitCollection(input);
	const Input& pgInput = visitor.getInput();

	result = PQprepare(pgConnection, "", query.c_str(), pgInput.size(), pgInput.getTypes());
	throwIfError();
	close(); // clear result

	result = PQexecPrepared(pgConnection, "", pgInput.size(), pgInput.getValues(), pgInput.getLengths(), pgInput.getFormats(), 1);

	const auto resultType = getResultType();
	throwIfError(resultType);

	iterator = 0;
	if (resultType == Data) {
		rows = PQntuples(pgResult);
		if (rows) {
			cols = PQnfields(pgResult);
		}
	} else {
		rows = 0;
		cols = 0;
	}

	const std::string str(PQcmdTuples(pgResult));
	if(str.empty()) {
		return 0;
	}

	return boost::lexical_cast<size_t>(str);
}

namespace {
using namespace Nov::Database::Types;

class ColInfo {
	PGresult* const result;
	const int col = 0;
	bool isBinary() const {
		return PQfformat(result, col);
	}
	Oid getType() const {
		return PQftype(result, col);
	}
public:
	explicit ColInfo(PGresult* r, int c):result(r), col(c) {
	}

	void validate(const std::vector<Oid>& expectedTypes) const {
		if (!isBinary()) {
			throwException("Expected all output data to be in binray form");
		}
		const auto type = getType();
		for (const auto expected : expectedTypes) {
			if (type == expected) return;
		}

		std::ostringstream ss;
		ss << "Expected column [" << col << "] to be of type ";
		bool first = true;
		for (const auto expected : expectedTypes) {
			if (first) {
				first = false;
			} else {
				ss << " or ";
			}
			ss << "oid=" << expected << "(" << PgTypes::name(expected) << ")";
		}
		ss << " but got oid="<< type << "(" << PgTypes::name(type) << ")";
		throwException(ss.str());
	}
};

class ValidateVisitor: public ParamReadVisitor {
	PGresult* result;
	int col = 0;

	void validateMany(const std::vector<Oid>& expectedTypes) {
		ColInfo info(result, col++);
		info.validate(expectedTypes);
	}

	void validate(Oid expectedType) {
		validateMany({expectedType});
	}
public:
	ValidateVisitor(PGresult* res):result(res) {
	}

	void operator()(const Types::String*) override {
		validateMany({PgTypes::Varchar, PgTypes::String, PgTypes::Char});
	}
	void operator()(const Types::Integer*) override {
		validateMany({PgTypes::Int32, PgTypes::Int64});
	}
	void operator()(const Types::Double*) override {
		validate(PgTypes::Double);
	}
	void operator()(const Types::Timestamp*) override {
		validate(PgTypes::DateTime);
	}
	void operator()(const Types::Binary*) override {
		validate(PgTypes::Binary);
	}
};

class OutputVisitor: public ParamWriteVisitor {
	PGresult* result;
	int col = 0;
	int row = 0;

	void to(Column& c, Types::Binary& x) {
		Types::BinaryData data;
		to(c, data);
		x.decode(data);
	}

	template<typename T>
	void to(Column& c, T& x) {
		c.to(x);
	}

	template<typename T>
	bool passNoInc(T& x) {
		Column c(result, row, col);
		if (c.isNull()) {
			return false;
		}
		to(c, x);
		return true;
	}

	template<typename T>
	bool pass(T& x) {
		const bool b = passNoInc(x);
		++col;
		return b;
	}
public:
	OutputVisitor(PGresult* res, int r):result(res), row(r) {
	}

	bool operator()(Types::String& v) override {
		return pass(v);
	}
	bool operator()(Types::Integer& v) override {
		return pass(v);
	}
	bool operator()(Types::Double& v) override {
		return pass(v);
	}
	bool operator()(Types::Timestamp& v) override {
		return pass(v);
	}
	bool operator()(Types::Binary& v) override {
		return pass(v);
	}
};

} // namespace

bool StatementImp::doFetch(OutputParams& output) {
	if (iterator >= rows) {
		return false;
	}

	if (cols != static_cast<int>(output.size())) {
		std::ostringstream ss;
		ss << "Expected result to have " << output.size() << "] columns, but fetch returned " << cols;
		throwException(ss.str());
	}

	ValidateVisitor validateVisitor(pgResult);
	validateVisitor.visitCollection(output);

	OutputVisitor outputVisitor(pgResult, iterator++);
	outputVisitor.visitCollection(output);

	return true;
}

const std::string StatementImp::errorMessage() const {
	return PQresultErrorMessage( pgResult );
}

void StatementImp::throwIfError() const {
	throwIfError(getResultType());
}

void StatementImp::throwIfError(ResultType resultType) const {
	if (resultType == FatalError) {
		throwException("fatal error - result is null");
	} else if (resultType == Error) {
		throwException(errorMessage());
	}
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
