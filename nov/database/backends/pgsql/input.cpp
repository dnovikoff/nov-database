#include <endian.h>
#include "endian.hpp"

#include "input.hpp"
#include "time.hpp"

namespace Nov {
namespace Database {
namespace PgSql {

void Input::addBuffer(const char * d, Oid t, int l) {
	type.push_back( t );
	data.push_back( d );
	length.push_back( l );
	formats.push_back( 1 );
}

void Input::addBuffer(buffer_t data, Oid type, int len) {
	buffers.push_back( data );
	addBuffer(reinterpret_cast<const char*>( &buffers.back() ), type, len);
}

Input::Input() {
	static const size_t reserveSize = 16;
	buffers.reserve( reserveSize );
	length.reserve( reserveSize );
	formats.reserve( reserveSize );
	data.reserve( reserveSize );
	type.reserve( reserveSize );
	binaries.reserve( reserveSize );
}

const char * const * Input::getValues() const {
	if(empty()) return nullptr;
	return data.data();
}

const int * Input::getLengths() const {
	if(empty()) return nullptr;
	return length.data();
}

const int * Input::getFormats() const {
	if(empty()) return nullptr;
	return formats.data();
}

const Oid * Input::getTypes() const {
	if(empty()) return nullptr;
	return type.data();
}

void Input::addNull(Oid type) {
	addBuffer(nullptr, type, 0);
}

void Input::add(int64_t v) {
	buffer_t b;
	b.i64 = htobe64(v);
	addBuffer(b, PgTypes::Int64, sizeof(int64_t));
}

void Input::add(double v) {
	buffer_t b;
	swap_endian( v );
	b.d = v;
	addBuffer(b, PgTypes::Double, sizeof(double));
}

void Input::add(const std::string& v) {
	addBuffer(v.c_str(), PgTypes::Varchar, static_cast<int>(v.length()));
}

void Input::add(const Types::BinaryData& v) {
	binaries.push_back(v);
	addBuffer(v.as<char>(), PgTypes::Binary, static_cast<int>(v.getLength()));
}

void Input::add(const Types::Timestamp& ts) {
	buffer_t b;
	b.i64 = htobe64(timestampToPgTime(ts));
	addBuffer(b, PgTypes::DateTime, sizeof(b.i64));
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
