#ifndef NOV_DATABASE_BACKENDS_PGSQL_TYPE2OID_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_TYPE2OID_HPP_

#include <stdint.h>
#include <string>
#include <nov/database/backends/pgsql/types.hpp>
#include <nov/database/types/binary.hpp>
#include <nov/database/types/timestamp.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

template<typename T>
struct Type2Oid {};

#define TYPE2OID(CTYPE, TYPE)\
template<>\
struct Type2Oid<CTYPE> {\
	static Oid value() { return PgTypes::TYPE; }\
}

TYPE2OID(int64_t, Int64);
TYPE2OID(std::string, Varchar);
TYPE2OID(double, Double);
TYPE2OID(Types::BinaryData, Binary);
TYPE2OID(Types::Timestamp, DateTime);

#undef TYPE2OID

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_TYPE2OID_HPP_
