#include "connection.hpp"
#include "statement.hpp"
#include "exception.hpp"
#include <nov/database/stream.hpp>
#include <nov/database/meta.hpp>
#include <nov/database/result.hpp>
#include <nov/database/stream_imp.hpp>

#include <libpq-fe.h>

namespace Nov {
namespace Database {
namespace PgSql {

static inline PGconn* pgConn(void* d) {
	return reinterpret_cast<PGconn*> ( d );
}

#define pgData pgConn(data)

bool ConnectionImp::checkConnection() const {
	return PQstatus( pgData ) == CONNECTION_OK;
}

const std::string ConnectionImp::errorString() const {
	return PQerrorMessage( pgData );
}

ConnectionImp::ConnectionImp(const std::string& connectInfo) {
	data = PQconnectdb(connectInfo.c_str());
	if (!checkConnection()) {
		const auto error = errorString();
		close();
		throwException(error);
	}
}

std::shared_ptr<Statement> ConnectionImp::newStatement(const std::string& query) {
	return std::make_shared<StatementImp>(query, data);
}

ConnectionImp::~ConnectionImp() {
	close();
}

void ConnectionImp::close() {
	if (data != nullptr) {
		PQfinish( pgData );
		data = nullptr;
	}
}

namespace {
class ParamToTypeConstVisitor: public ParamReadVisitor {
public:
	std::string resultType;

	void operator()(const Types::String*) override {
		resultType = "text";
	}
	void operator()(const Types::Integer*) override {
		resultType = "integer";
	}
	void operator()(const Types::Double*) override {
		resultType = "double precision";
	}
	void operator()(const Types::Timestamp*) override {
		resultType = "timestamptz";
	}
	void operator()(const Types::Binary*) override {
		resultType = "bytea";
	}
};

} // namespace

void ConnectionImp::createTable(const Meta& meta) {
	Stream query = createStream();
	query << "CREATE TABLE " << meta.getTable() << " (";
	bool firstField = true;
	for (const auto& field: meta.getAssignedFields()) {
		if (firstField) {
			firstField = false;
		} else {
			query << ", ";
		}
		std::string type;
		if (meta.getSerial() == field.getField()) {
			type = "serial";
		} else {
			ParamToTypeConstVisitor vis;
			field.visitParamForRead(vis);
			type = vis.resultType;
		}

		query << field.getField() << " " << type.c_str();
	}

	for (auto key: meta.getKeys()) {
		if (key.getType() == Key::Index) continue;
		query << ", ";

		switch (key.getType()) {
			case Key::Primary:
				query << "PRIMARY KEY";
				break;
			case Key::Unique:
				query << "UNIQUE";
				break;
			default:
				break;
		}
		query << "(";
		writeKeyFields(query, key);
		query << ")";
	}
	query << ")";
	query.execute();
}

void ConnectionImp::createKeys(const Meta& meta) {
	for (auto key: meta.getKeys()) {
		if (key.getType() != Key::Index) continue;
		Stream query = createStream();
		query << "CREATE INDEX ON " << meta.getTable() << "(";
		writeKeyFields(query, key);
		query << ")";
		query.execute();
	}

	size_t keyNum = 0;
	for (auto fk: meta.getForeignKeys()) {
		const auto& key = fk.key;
		Stream query = createStream();
		std::ostringstream keyName;
		keyName << "fk_" << (++keyNum) << "_" << meta.getTable().name();
		query << "ALTER TABLE "
			<< meta.getTable()
			<< " ADD CONSTRAINT "
			<< Field(keyName.str())
			<< " FOREIGN KEY ("
		;
		writeKeyFields(query, key);
		query << ") REFERENCES " << fk.meta->getTable() << "(";
		writeKeyFields(query, fk.meta->getPrimaryKey());
		query << ")";
		query.execute();
	}
}

void ConnectionImp::executeInsertAndReturnId(Stream& insertQuery, const Meta& meta, ParamRef& returnId) {
	insertQuery << " RETURNING " << meta.getSerial().bind(returnId);
	insertQuery.execute().fetch();
}

namespace {

class PgStreamImp: public StreamImp {
	virtual void writePlaceholder() override {
		out << "$" << getPlaceholderNumber();
	}
};

}  // namespace

StreamImp* ConnectionImp::newStreamImp() {
	return new PgStreamImp;
}

static const std::string commitCommand = "COMMIT";
static const std::string rollbackCommand = "ROLLBACK";
static const std::string startCommand = "BEGIN";

const std::string& ConnectionImp::startTransactionCommand() const {
	return startCommand;
}

const std::string& ConnectionImp::commitTransactionCommand() const {
	return commitCommand;
}

const std::string& ConnectionImp::rollbackTransactionCommand() const {
	return rollbackCommand;
}

Database::BindedResult ConnectionImp::doExecuteUpdateReturning(Stream& updateQuery, const Meta&, const BindedFields& binded) {
	updateQuery << " RETURNING " << binded;
	return updateQuery.execute();
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
