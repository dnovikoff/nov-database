#ifndef NOV_DATABASE_BACKENDS_PGSQL_STATEMENT_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_STATEMENT_HPP_

#include <nov/database/statement.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

class StatementImp: public Statement {
	enum ResultType {
		FatalError,
		// Error, during execution
		Error,
		// This query does not expect to return data (ex. insert/update/delete without RETURNING statement)
		NoData,
		// This query expectes to return data. Althought it could return 0 rows
		Data
	};

	void close();
	const std::string errorMessage() const;
	void throwIfError() const;
	void throwIfError(ResultType resType) const;

	void doPrepare(const std::string& q) override;
	size_t doExecute(const InputParams& input) override;
	bool doFetch(OutputParams& output) override;

	void* result = nullptr;
	void* connection = nullptr;

	int iterator = 0;
	int rows = 0;
	int cols = 0;

	std::string query;

	ResultType getResultType() const;
	const char * getDataAt(int c) const;
public:
	StatementImp(const std::string& query, void* con);
	~StatementImp();
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_STATEMENT_HPP_
