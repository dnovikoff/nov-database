#ifndef NOV_DATABASE_BACKENDS_PGSQL_INPUT_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_INPUT_HPP_

#include <string>
#include <vector>
#include <nov/database/backends/pgsql/type2oid.hpp>
#include <nov/database/types/binary.hpp>
#include <nov/database/types/timestamp.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

class Input {
	union buffer_t {
		int64_t i64;
		double d;
	};
	std::vector<Types::BinaryData> binaries;
	std::vector<buffer_t> buffers;
	std::vector<Oid> type;
	std::vector<int> length;
	std::vector<int> formats;
	std::vector<const char*> data;

	Input(const Input&) = delete;
	Input& operator=(const Input&) = delete;

	void addBuffer(const char * data, Oid type, int len);
	void addBuffer(buffer_t data, Oid type, int len);

	// Add null value of the specified type
	void addNull(Oid type);
public:
	Input();

	// Get arguments for libpq prepare functions in required format
	//@{
	const char * const * getValues() const;
	const int * getLengths() const;
	const int * getFormats() const;
	const Oid * getTypes() const;
	//@}

	size_t size() const { return data.size(); }
	bool empty() const { return data.empty(); }

	// Add data of specific type
	// Will copy and hold value
	//@{
	void add(int64_t v);
	void add(const Types::Timestamp& t);
	void add(double v);
	//@}

	void add(const std::string& v);
	void add(const Types::BinaryData& v);

	template<typename T>
	void addNull() {
		addNull(Type2Oid<T>::value());
	}
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_INPUT_HPP_
