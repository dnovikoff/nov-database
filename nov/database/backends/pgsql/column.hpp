#ifndef NOV_DATABASE_BACKENDS_PGSQL_COLUMN_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_COLUMN_HPP_

#include <string>
#include <nov/database/types/timestamp.hpp>
#include <nov/database/types/binary.hpp>

namespace Nov {
namespace Database {
namespace PgSql {
/**
 * Holds row+col positions at results set
 *
 * lazy-initializes type if needed
 */
class Column {
	void * data = nullptr;
	int row = 0;
	int col = 0;
public:
	Column(void * d, int row, int col);
	/**
	 * Is column have a null value
	 */
	bool isNull();

	/**
	 * Column length. For primitive types it will be equals size
	 * For varchars it will show real char length
	 */
	int length();

	/**
	 * Returns pointer to data. No special checks
	 */
	const char * getData();

	Oid getType() const;

	/**
	 * Return data as one of this types
	 * Note that type should match exactly
	 * Will throw in case of different type
	 */
	// @{
	void to(int32_t& x);
	void to(int64_t& x);
	void to(double& x);
	void to(Types::Timestamp& x);
	void to(std::string& x);
	void to(Types::BinaryData& x);
	// }@
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_COLUMN_HPP_
