#ifndef NOV_DATABASE_BACKENDS_PGSQL_CONFIGURATION_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_CONFIGURATION_HPP_

#include <string>

#include <nov/database/configuration.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

class ConfigurationImp: public Configuration {
	const std::string connectionInfo;
public:
	explicit ConfigurationImp(const std::string& info);
	std::shared_ptr<Connection> newConnection() override;
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_CONFIGURATION_HPP_
