#ifndef NOV_DATABASE_BACKENDS_PGSQL_TYPES_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_TYPES_HPP_

#include <postgres_ext.h>

namespace Nov {
namespace Database {
namespace PgSql {

struct PgTypes {
	static const Oid Int16;
	static const Oid Int32;
	static const Oid Int64;
	static const Oid Double;
	static const Oid Numeric;
	static const Oid Char;
	static const Oid Varchar;
	static const Oid String;
	static const Oid Binary;
	static const Oid DateTime;

	// This will be returned for example if you will (SELECT 'hello'). It cant guess the type for such attributes
	// except for type is set with ::
	static const Oid Unknown;

	static const char* name(Oid type);
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_TYPES_HPP_
