#include <algorithm>

#include <libpq-fe.h>
#include "column.hpp"
#include "endian.hpp"
#include "types.hpp"
#include "time.hpp"

namespace Nov {
namespace Database {
namespace PgSql {

static inline PGresult* pgResult(void* d) {
	return reinterpret_cast<PGresult*> ( d );
}

#define pgData pgResult(data)

Column::Column(void * d, int r, int c):data(d), row(r), col(c) {}

bool Column::isNull() {
	return PQgetisnull(pgData, row, col);
}

int Column::length() {
	return PQgetlength(pgData, row, col);
}

const char * Column::getData() {
	return PQgetvalue(pgData, row, col);
}

Oid Column::getType() const {
	return PQftype(pgData, col);
}

void Column::to(int32_t& x) {
	const int32_t v = *reinterpret_cast<const int32_t*>(getData());
	x = be32toh(v);
}

void Column::to(int64_t& x) {
	if (getType() == PgTypes::Int32) {
		int32_t x32 = 0;
		to(x32);
		x = static_cast<int64_t>(x32);
		return;
	}
	const int64_t v = *reinterpret_cast<const int64_t*>(getData());
	x = be64toh(v);
}

void Column::to(double& x) {
	x = *reinterpret_cast<const double*>(getData());
	swap_endian(x);
}

void Column::to(Types::Timestamp& x) {
	int64_t i64 = 0;
	to(i64);
	x = pgTimeToTimestamp(i64);
}

void Column::to(std::string& x) {
	std::string(getData(), length()).swap(x);
}

void Column::to(Types::BinaryData& x) {
	x = Types::BinaryData::ref(static_cast<size_t>(length()), getData());
}

} // namespace PgSql
} // namespace Database
} // namespace Nov
