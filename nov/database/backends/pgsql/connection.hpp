#ifndef NOV_DATABASE_BACKENDS_PGSQL_CONNECTION_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_CONNECTION_HPP_

#include <nov/database/connection.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

class ConnectionImp: public Connection {
	void executeInsertAndReturnId(Stream& insertQuery, const Meta& meta, ParamRef& returnId) override;
public:
	explicit ConnectionImp(const std::string& connectInfo);
	std::shared_ptr<Statement> newStatement(const std::string& query) override;
	~ConnectionImp();

	void createTable(const Meta& tableMeta) override;
	void createKeys(const Meta& tableMeta) override;
private:
	virtual const std::string& startTransactionCommand() const override;
	virtual const std::string& commitTransactionCommand() const override;
	virtual const std::string& rollbackTransactionCommand() const override;

	Database::BindedResult doExecuteUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded) override;

	void close();
	virtual class StreamImp* newStreamImp();
	const std::string errorString() const;
	bool checkConnection() const;
	void* data = nullptr;
};

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_CONNECTION_HPP_
