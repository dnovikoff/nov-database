#ifndef NOV_DATABASE_BACKENDS_PGSQL_ENDIAN_HPP_
#define NOV_DATABASE_BACKENDS_PGSQL_ENDIAN_HPP_

#include <algorithm>
#include <boost/detail/endian.hpp>

namespace Nov {
namespace Database {
namespace PgSql {

namespace {
#ifdef BOOST_LITTLE_ENDIAN
template <typename T>
inline void swap_endian(T& pX) {
	char& raw = reinterpret_cast<char&>(pX);
	std::reverse(&raw, &raw + sizeof(T));
}
#elif defined(BOOST_BIG_ENDIAN)
template <typename T> inline void swap_endian(T&) {}
#else
  #error "unable to determine system endianness"
#endif
} // namespace

} // namespace PgSql
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_PGSQL_ENDIAN_HPP_
