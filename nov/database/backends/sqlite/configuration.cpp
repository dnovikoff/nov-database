#include "configuration.hpp"
#include "connection.hpp"

namespace Nov {
namespace Database {
namespace Sqlite {

std::shared_ptr<Connection> ConfigurationForFiles::newConnection() {
	return std::make_shared<ConnectionImp>("file:" + fileName);
}

std::shared_ptr<Connection> ConfigurationForMemory::newConnection() {
	return std::make_shared<ConnectionImp>("file:" + id + "?mode=memory&cache=shared");
}

std::shared_ptr<Connection> ConfigurationTemp::newConnection() {
	return std::make_shared<ConnectionImp>(":memory:");
}

} // namespace Sqlite
} // namespace Database
} // namespace Nov
