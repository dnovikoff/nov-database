#include <boost/lexical_cast.hpp>

#include "statement.hpp"
#include "sqlite3.h"
#include "exception.hpp"
#include <nov/database/param.hpp>

namespace Nov {
namespace Database {
namespace Sqlite {

StatementImp::StatementImp(const std::string& query, sqlite3* con):Statement(query), connection(con) {}

StatementImp::~StatementImp() {
	close();
}

void StatementImp::close() {
	if (data != nullptr) {
		sqlite3_finalize(data);
	}
}

/**
 * Statement could not be executed separately from fetch statements
 * So we should do nothing on first fetch (fetch of first row is done in execute phase)
 */
bool StatementImp::step() {
	if (stepStatus == DONE) return false;
	if (stepStatus == FIRST_ROW_FETCHED) {
		stepStatus = PROGRESS;
		return true;
	}
	const int code = sqlite3_step(data);
	if (code == SQLITE_DONE) {
		stepStatus = DONE;
		return false;
	} else if (code == SQLITE_ROW) {
		if (stepStatus == NOT_EXECUTED) {
			stepStatus = FIRST_ROW_FETCHED;
		} else {
			stepStatus = PROGRESS;
		}
		return true;
	}
	needToReset = true;
	Exception::throwException(connection);
	return false;
}

void StatementImp::doPrepare(const std::string& query) {
	close();
	const char * pzTail = nullptr;
	const int code = sqlite3_prepare_v2(connection, query.c_str(), -1, &data, &pzTail);
	Exception::throwIfNeeded(connection, code);
	needToReset = false;
}

size_t StatementImp::doExecute(const InputParams& input) {
	const size_t bindCount = static_cast<size_t>(sqlite3_bind_parameter_count( data ));
	if (bindCount != input.size()) {
		std::ostringstream oss;
		oss << "Database expects " << bindCount << " columns to be binded, but user provided " << input.size();
		throw Exception(oss.str());
	}
	if(needToReset) {
		sqlite3_reset(data);
	} else {
		// No need to reset if its the first query
		// Or already reseted to get error result
		needToReset = true; // will reset next time
	}

	bind(input);

	stepStatus = NOT_EXECUTED;
	step();
	return static_cast<size_t>(sqlite3_changes(connection));
}

namespace {
using namespace Nov::Database::Types;

class InputParamConstVisitor: public ParamReadVisitor {
	sqlite3* const connection;
	sqlite3_stmt* const stmt;
	size_t index = 1; // First index for sqlite is 1 - not 0

	void check(int code) {
		Exception::throwIfNeeded(connection, code);
	}

	void bind(const Double& val) {
		check(sqlite3_bind_double(stmt, index, val));
	}
	void bind(const String& val, const bool copy = false) {
		check(sqlite3_bind_text(stmt, index, val.c_str(), static_cast<int>(val.size()), copy?SQLITE_TRANSIENT:SQLITE_STATIC));
	}
	void bind(const Timestamp& val) {
		bind(boost::lexical_cast<std::string>(val), true);
	}
	void bind(const Integer& val) {
		check(sqlite3_bind_int64(stmt, index, val));
	}
	void bind(const BinaryData& val) {
		check(sqlite3_bind_blob(stmt, index, val.as<void>(), static_cast<int>(val.getLength()), SQLITE_TRANSIENT));
	}
	void bind(const Binary& val) {
		bind(val.encode());
	}
	void bindNull() {
		check(sqlite3_bind_null(stmt, index));
	}
	template<typename T>
	void pass(const T* data) {
		if (data == nullptr) {
			bindNull();
		} else {
			bind(*data);
		}
		++index;
	}
public:
	InputParamConstVisitor(sqlite3* c, sqlite3_stmt* s):connection(c), stmt(s) {}

	void operator()(const Types::String* value) override {
		pass(value);
	}
	void operator()(const Types::Integer* value) override {
		pass(value);
	}
	void operator()(const Types::Double* value) override {
		pass(value);
	}
	void operator()(const Types::Timestamp* value) override {
		pass(value);
	}
	void operator()(const Types::Binary* value) override {
		pass(value);
	}
};

} // namespace

void StatementImp::bind(const InputParams& input) {
	InputParamConstVisitor visitor(connection, data);
	visitor.visitCollection(input);
}

namespace {
using namespace Nov::Database::Types;

class OutputParamVisitor: public ParamWriteVisitor {
	sqlite3* const connection;
	sqlite3_stmt* const stmt;
	size_t index = 0;
	int tempColumnType = 0;

	size_t bytes() {
		return static_cast<size_t>(sqlite3_column_bytes(stmt, index));
	}

	void checkType(const int expected) {
		if (tempColumnType == expected) return;

		std::ostringstream ss;
		ss << "Wrong type for column " << index << ". Real type of column is " << tempColumnType << ". But type " << expected << " is expected by user.";
		throw Exception( ss.str() );
	}

	void set(Double& val) {
		checkType(SQLITE_FLOAT);
		val = sqlite3_column_double(stmt, index);
	}
	void set(String& val) {
		checkType(SQLITE_TEXT);
		const char * data = reinterpret_cast<const char*>(sqlite3_column_text(stmt, index));
		val.assign(data, bytes());
	}
	void set(Timestamp& val) {
		String tmp;
		set(tmp);
		val = boost::lexical_cast<Timestamp>(tmp);
	}
	void set(Integer& val) {
		checkType(SQLITE_INTEGER);
		val = sqlite3_column_int(stmt, index);
	}
	void set(Binary& val) {
		checkType(SQLITE_BLOB);
		const void* data = sqlite3_column_blob(stmt, index);
		val.decode(BinaryData::ref(bytes(), data));
	}
	void setNull() {
		throw Exception("No null values are now allowed");
	}

	template<typename T>
	bool passNoInc(T& value) {
		tempColumnType = sqlite3_column_type(stmt, index);
		if (tempColumnType == SQLITE_NULL) {
			return false;
		}
		set(value);
		return true;
	}
	template<typename T>
	bool pass(T& value) {
		const bool b = passNoInc(value);
		++index;
		return b;
	}
public:
	OutputParamVisitor(sqlite3* c, sqlite3_stmt* s):connection(c), stmt(s) {}

	bool operator()(Types::String& value) override {
		return pass(value);
	}
	bool operator()(Types::Integer& value) override {
		return pass(value);
	}
	bool operator()(Types::Double& value) override {
		return pass(value);
	}
	bool operator()(Types::Timestamp& value) override {
		return pass(value);
	}
	bool operator()(Types::Binary& value) override {
		return pass(value);
	}
};

} // namespace

bool StatementImp::doFetch(OutputParams& output) {
	if (!step()) return false;

	const size_t columnCount = static_cast<size_t>(sqlite3_column_count(data));
	if (columnCount != output.size()) {
		std::ostringstream oss;
		oss << "Database returned " << columnCount << " columns, but user expects  " << output.size();
		throw Exception(oss.str());
	}

	OutputParamVisitor visitor(connection, data);
	visitor.visitCollection(output);

	return true;
}

} // namespace Sqlite
} // namespace Database
} // namespace Nov
