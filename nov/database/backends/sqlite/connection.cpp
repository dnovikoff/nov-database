#include <sqlite3.h>
#include "connection.hpp"
#include "statement.hpp"
#include "exception.hpp"
#include <nov/database/stream.hpp>
#include <nov/database/meta.hpp>
#include <nov/database/result.hpp>

namespace Nov {
namespace Database {
namespace Sqlite {

ConnectionImp::ConnectionImp(const std::string& fileName) {
	const int code = sqlite3_open_v2(fileName.c_str(), &data, SQLITE_OPEN_READWRITE | SQLITE_OPEN_URI, nullptr);
	if(code != SQLITE_OK) {
		throw Exception( "Could not connect to " + fileName );
	}
}

std::shared_ptr<Statement> ConnectionImp::newStatement(const std::string& query) {
	return std::make_shared<StatementImp>(query, data);
}

ConnectionImp::~ConnectionImp() {
	sqlite3_close(data);
}

namespace {
class ParamToTypeConstVisitor: public ParamReadVisitor {
public:
	std::string resultType;

	void operator()(const Types::String*) override {
		resultType = "TEXT";
	}
	void operator()(const Types::Integer*) override {
		resultType = "INTEGER";
	}
	void operator()(const Types::Double*) override {
		resultType = "REAL";
	}
	void operator()(const Types::Timestamp*) override {
		resultType = "TEXT";
	}
	void operator()(const Types::Binary*) override {
		resultType = "BLOB";
	}
};
} // namespace

static const char utilityLastId[] = "UtilityLastId";

void ConnectionImp::createUtility() {
	Stream query = createStream();
	query << "CREATE TABLE IF NOT EXISTS " << utilityLastId << " (Name TEXT PRIMARY KEY, Id INTEGER)";
	query.execute();
}

void ConnectionImp::initUtility(const Meta& tableMeta) {
	if (!utilityCreated) {
		createUtility();
		utilityCreated = true;
	}

	const auto& name = tableMeta.getTable().name();
	const char* const cName = name.c_str();

	{
		Stream query = createStream();
		query << "DELETE FROM " << utilityLastId << " WHERE Name = " << name;
		query.execute();
	}
	{
		Stream query = createStream();
		query << "INSERT INTO " << utilityLastId << " VALUES (" << name << ", 0)";
		query.execute();
	}
	{
		Stream query = createStream();
		query <<
			"CREATE TRIGGER IF NOT EXISTS UpdateTrigger" << cName << " AFTER UPDATE ON " << cName <<
			" BEGIN"
			" UPDATE " << utilityLastId << " SET Id = new.rowid WHERE Name = '" << cName << "';"
			" END"
		;
		query.execute();
	}
}

void ConnectionImp::createTable(const Meta& meta) {
	Stream query = createStream();
	query << "CREATE TABLE " << meta.getTable() << " (";
	bool firstField = true;
	for (const auto& field: meta.getAssignedFields()) {
		if (firstField) {
			firstField = false;
		} else {
			query << ", ";
		}
		std::string type;
		if (meta.getSerial() == field.getField()) {
			type = "INTEGER PRIMARY KEY AUTOINCREMENT";
		} else {
			ParamToTypeConstVisitor vis;
			field.visitParamForRead(vis);
			type = vis.resultType;
		}

		query << field.getField() << " " << type.c_str();
	}

	for (auto key: meta.getKeys()) {
		if (key.getType() != Key::Primary) continue;
		if (!meta.getSerial().name().empty()) {
			if (key.getFields().size() > 1) {
				key.setType(Key::Unique);
			} else {
				continue;
			}
		}

		query << ", PRIMARY KEY(";
		writeKeyFields(query, key);
		query << ")";
	}

	for (const auto& fk: meta.getForeignKeys()) {
		query << ", FOREIGN KEY(";
		writeKeyFields(query, fk.key);
		query << ") REFERENCES " << fk.meta->getTable() << " (";
		writeKeyFields(query, fk.meta->getPrimaryKey());
		query << ")";
	}
	query << ")";
	query.execute();
	initUtility(meta);
}
// CREATE INDEX file_hash_list_filesize_idx ON file_hash_list (filesize);
// CREATE UNIQUE INDEX IF NOT EXISTS MyUniqueIndexName ON auth_user (email)

void ConnectionImp::createKeys(const Meta& meta) {
	size_t idx = 0;
	for (auto key: meta.getKeys()) {
		if (key.getType() == Key::Primary) continue;
		++idx;
		Stream query = createStream();
		std::ostringstream oss;
		oss << meta.getTable().name() << "_idx" << idx;
		query << "CREATE ";
		switch (key.getType()) {
			case Key::Index:
				query << "INDEX";
				break;
			case Key::Unique:
				query << "UNIQUE";
				break;
			default:
				throw std::logic_error("Unsupported key type");
				break;
		}
		query << " " << Field(oss.str()) << " ON " << meta.getTable() << "(";
		writeKeyFields(query, key);
		query << ")";
	}
}

void ConnectionImp::executeInsertAndReturnId(Stream& insertQuery, const Meta& meta, ParamRef& returnId) {
	insertQuery.execute();
	static const Field seqField("seq");
	static const Field nameField("name");
	static const Field seqTable("sqlite_sequence");

	Stream s = createStream();
	s << "SELECT "
		<< seqField.bind(returnId)
		<< " FROM " << seqTable
		<< " WHERE " << nameField.assignCopy(meta.getTable().name())
	;
	s.execute().fetch();
}

static const std::string commitCommand = "COMMIT";
static const std::string rollbackCommand = "ROLLBACK";
static const std::string startCommand = "BEGIN";

const std::string& ConnectionImp::startTransactionCommand() const {
	return startCommand;
}

const std::string& ConnectionImp::commitTransactionCommand() const {
	return commitCommand;
}

const std::string& ConnectionImp::rollbackTransactionCommand() const {
	return rollbackCommand;
}

BindedResult ConnectionImp::doExecuteUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded) {
	auto updateResult = updateQuery.execute();
	if (updateResult.getAffacted() == 0) {
		while (updateResult.fetch()) {
		}
		return updateResult;
	}
	auto query = createStream();
	query << "SELECT " << binded << " FROM " << meta.getTable() << " WHERE rowid = (SELECT Id FROM " << utilityLastId << " WHERE Name=" << meta.getTable().name() << " LIMIT 1) LIMIT 1";
	return query.execute();
}

} // namespace Sqlite
} // namespace Database
} // namespace Nov
