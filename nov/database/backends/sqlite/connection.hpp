#ifndef NOV_DATABASE_BACKENDS_SQLITE_CONNECTION_HPP_
#define NOV_DATABASE_BACKENDS_SQLITE_CONNECTION_HPP_

#include <nov/database/connection.hpp>

namespace Nov {
namespace Database {
namespace Sqlite {

class ConnectionImp: public Connection {
public:
	explicit ConnectionImp(const std::string& fileName);
	std::shared_ptr<Statement> newStatement(const std::string& query) override;
	~ConnectionImp();

	void createTable(const Meta& tableMeta) override;
	void createKeys(const Meta& tableMeta) override;
private:
	void createUtility();
	void initUtility(const Meta& tableMeta);
	void executeInsertAndReturnId(Stream& insertQuery, const Meta& meta, ParamRef& returnId) override;
	BindedResult doExecuteUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded) override;

	virtual const std::string& startTransactionCommand() const override;
	virtual const std::string& commitTransactionCommand() const override;
	virtual const std::string& rollbackTransactionCommand() const override;

	struct sqlite3* data = nullptr;
	bool utilityCreated = false;
};

} // namespace Sqlite
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_SQLITE_CONNECTION_HPP_
