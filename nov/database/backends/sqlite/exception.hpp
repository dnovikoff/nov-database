#ifndef NOV_DATABASE_BACKENDS_SQLITE_EXCEPTION_HPP_
#define NOV_DATABASE_BACKENDS_SQLITE_EXCEPTION_HPP_

#include <nov/database/exception.hpp>

struct sqlite3;

namespace Nov {
namespace Database {
namespace Sqlite {

class Exception: public Database::Exception {
	typedef Database::Exception parent_t;
public:
	explicit Exception(const std::string& message);

	static void throwException(sqlite3* connect);
	static void throwIfNeeded(sqlite3* connect, int code);
};

} // namespace Sqlite
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_SQLITE_EXCEPTION_HPP_
