#ifndef NOV_DATABASE_BACKENDS_SQLITE_STATEMENT_HPP_
#define NOV_DATABASE_BACKENDS_SQLITE_STATEMENT_HPP_

#include <nov/database/statement.hpp>

struct sqlite3_stmt;
struct sqlite3;

namespace Nov {
namespace Database {
namespace Sqlite {

class StatementImp: public Statement {
	void close();

	// Returned data
	bool step();
	void bind(const InputParams& input);

	void doPrepare(const std::string& query) override;
	size_t doExecute(const InputParams& input) override;
	bool doFetch(OutputParams& output) override;

	struct sqlite3_stmt* data = nullptr;
	struct sqlite3* const connection;
	bool needToReset = false;
	enum StepStatus {NOT_EXECUTED, FIRST_ROW_FETCHED, PROGRESS, DONE} stepStatus = NOT_EXECUTED;

public:
	StatementImp(const std::string& query, sqlite3* con);
	~StatementImp();
};

} // namespace Sqlite
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_SQLITE_STATEMENT_HPP_
