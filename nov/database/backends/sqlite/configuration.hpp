#ifndef NOV_DATABASE_BACKENDS_SQLITE_CONFIGURATION_HPP_
#define NOV_DATABASE_BACKENDS_SQLITE_CONFIGURATION_HPP_

#include <string>

#include <nov/database/configuration.hpp>

namespace Nov {
namespace Database {
namespace Sqlite {

/**
 * Will create connections with databases in files
 */
class ConfigurationForFiles: public Configuration {
	const std::string fileName;
public:
	explicit ConfigurationForFiles(const std::string& f):fileName(f) {}
	std::shared_ptr<Connection> newConnection() override;
};

/**
 * Will create databases in memory, that will be shared by the given id
 */
class ConfigurationForMemory: public Configuration {
	const std::string id;
public:
	explicit ConfigurationForMemory(const std::string& i):id(i) {}
	std::shared_ptr<Connection> newConnection() override;
};

/**
 * Will create memory databases with no id. Could nbot be shared
 */
class ConfigurationTemp: public Configuration {
public:
	std::shared_ptr<Connection> newConnection() override;
};

} // namespace Sqlite
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKENDS_SQLITE_CONFIGURATION_HPP_
