#include <sstream>

#include "exception.hpp"
#include "sqlite3.h"

namespace Nov {
namespace Database {
namespace Sqlite {

void Exception::throwException(struct sqlite3* connect) {
	std::ostringstream ss;
	ss << "Error [" << sqlite3_errmsg( connect ) << "]";
	Database::throwException(ss.str());
}

void Exception::throwIfNeeded(struct sqlite3* connect, int code) {
	if( code == SQLITE_OK) return;
	throwException(connect);
}

Exception::Exception(const std::string& message): parent_t("sqlite3 error: " + message) {}

} // namespace Sqlite
} // namespace Database
} // namespace Nov
