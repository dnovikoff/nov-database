#include "operation.hpp"
#include "stream.hpp"
#include "result.hpp"
#include "meta.hpp"

namespace Nov {
namespace Database {

Operation::Operation(const Meta& m, const std::shared_ptr<Connection>& c): meta(m), connection(c) {
}

const Field& Operation::getTable() const {
	return meta.getTable();
}

BindedResult Operation::load(const Binded& output, const Assigned& where) {
	Stream stream = connection->createStream();
	stream <<
		"SELECT " << output <<
		" FROM " << getTable()
	;

	if (!where.empty()) {
		stream << " WHERE " << AssignList(" AND ", where);
	}

	return stream.execute();
}

bool Operation::exists(const Assigned& where) {
	Stream stream = connection->createStream();
	stream << "SELECT 1 FROM " << getTable();

	if (!where.empty()) {
		stream << " WHERE " << AssignList(" AND ", where);
	}

	stream << " LIMIT 1";
	Types::Integer tmp;
	stream.bind(tmp);

	return stream.execute().fetch();
}

void Operation::insert(const Assigned& input) {
	connection->insert(meta, input);
}

void Operation::insert(const Assigned& input, ParamRef id) {
	connection->insert(meta, input, id);
}

size_t Operation::remove(const Assigned& where) {
	Stream stream = connection->createStream();
	stream <<
		"DELETE FROM " << getTable() <<
		" WHERE " << AssignList(" AND ", where)
	;

	return stream.execute().getAffacted();
}

size_t Operation::update(const Assigned& input, const Assigned& where) {
	Stream stream = connection->createStream();
	stream <<
		"UPDATE " << getTable() <<
		" SET " << AssignList(", ", input) <<
		" WHERE " << AssignList(" AND ", where)
	;

	return stream.execute().getAffacted();
}

} // namespace Database
} // namespace Nov
