#ifndef NOV_DATABASE_CONNECTION_HPP_
#define NOV_DATABASE_CONNECTION_HPP_

#include <memory>
#include <nov/database/param.hpp>
#include <nov/database/field.hpp>

namespace Nov {
namespace Database {

class Statement;
class Stream;
class Result;
class Stream;
class Meta;
class BindedResult;

class Connection: public std::enable_shared_from_this<Connection> {
	Connection(const Connection&) = delete;
	Connection& operator=(const Connection&) = delete;

	// Default implementation
	virtual class StreamImp* newStreamImp();
	virtual void executeInsertAndReturnId(Stream& insertQuery, const Meta& meta, ParamRef& returnId) = 0;
	virtual const std::string& startTransactionCommand() const = 0;
	virtual const std::string& commitTransactionCommand() const = 0;
	virtual const std::string& rollbackTransactionCommand() const = 0;
	virtual BindedResult doExecuteUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded) = 0;
public:
	Connection();
	virtual std::shared_ptr<Statement> newStatement(const std::string& query) = 0;
	virtual void createTable(const Meta& tableMeta) = 0;
	virtual void createKeys(const Meta& tableMeta) = 0;

	void insert(const Meta& meta, const AssignedFields& params, ParamRef returnId);
	void insert(const Meta& meta, const AssignedFields& params);

	Result execute(const std::string& query);

	Stream createStream();

	virtual ~Connection() = 0;
	void startTransaction();
	void commitTransaction();
	void rollbackTransaction();

	bool executeUpdateReturning(Stream& updateQuery, const Meta& meta, const BindedFields& binded);
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_CONNECTION_HPP_
