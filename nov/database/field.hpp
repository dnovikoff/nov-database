#ifndef NOV_DATABASE_FIELD_HPP_
#define NOV_DATABASE_FIELD_HPP_

#include <string>
#include <vector>
#include <nov/database/param.hpp>

namespace Nov {
namespace Database {

class BindedField;
class AssignedField;

// Used in Stream queries for correct quoting
class Field {
	friend class Stream;
	std::string fieldName;
public:
	const std::string& name() const { return fieldName; }
	explicit Field(const std::string& name);
	const BindedField bind(const ParamRef& param) const;
	const AssignedField assignCopy(const ParamCopy& param) const;
	const AssignedField assignRef(const ParamRef& param) const;
	const AssignedField assign(const Param& param) const;
	bool operator==(const Field& rhs) const {
		return name() == rhs.name();
	}
	bool operator<(const Field& rhs) const {
		return name() < rhs.name();
	}
};

// It adds bind parameter with field name to string
// usage scenario :
// stream << " SELECT " << Field("my").bind(param) << " FROM " << ....
//
class BindedField {
	friend class Field;
	Field field;
	ParamRef param;

	BindedField(const Field& f, const ParamRef& p);
public:
	const Field& getField() const {
		return field;
	}
	const ParamRef& getParam() const {
		return param;
	}
	void visitParamForRead(ParamReadVisitor&) const;
};

class AssignedField {
	friend class Field;
	Field field;
	Param param;
	bool assign = false;

	AssignedField(const Field& f, const Param& p);
public:
	const Field& getField() const {
		return field;
	}
	const Param& getParam() const {
		return param;
	}
	void visitParamForRead(ParamReadVisitor&) const;
};

typedef std::vector<BindedField> BindedFields;
typedef std::vector<Field> Fields;
typedef std::vector<AssignedField> AssignedFields;

class AssignList {
	const std::string& separator;
	const AssignedFields& data;
public:
	AssignList(const std::string& sep, const AssignedFields& d);

	const std::vector<AssignedField>& getAssignedFields() const {
		return data;
	}

	const std::string& getSeparator() const {
		return separator;
	}
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_FIELD_HPP_
