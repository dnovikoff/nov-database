#ifndef NOV_DATABASE_PARAM_VISITOR_HPP_
#define NOV_DATABASE_PARAM_VISITOR_HPP_

#include <nov/database/types/integer.hpp>
#include <nov/database/types/timestamp.hpp>
#include <nov/database/types/double.hpp>
#include <nov/database/types/string.hpp>
#include <nov/database/types/binary.hpp>

namespace Nov {
namespace Database {

class ParamReadVisitor {
public:
	virtual void operator()(const Types::String* value) = 0;
	virtual void operator()(const Types::Integer* value) = 0;
	virtual void operator()(const Types::Double* value) = 0;
	virtual void operator()(const Types::Timestamp* value) = 0;
	virtual void operator()(const Types::Binary* value) = 0;

	template<typename T>
	void visitCollection(const T& c) {
		for (const auto& x: c) {
			x.visitForRead(*this);
		}
	}
	virtual ~ParamReadVisitor() = 0;
};

class ParamWriteVisitor {
public:
	// Should return null in case want to write null value
	// @{
	virtual bool operator()(Types::String& value) = 0;
	virtual bool operator()(Types::Integer& value) = 0;
	virtual bool operator()(Types::Double& value) = 0;
	virtual bool operator()(Types::Timestamp& value) = 0;
	virtual bool operator()(Types::Binary& value) = 0;
	// @}

	template<typename T>
	void visitCollection(const T& c) {
		for (auto x: c) {
			x.visitForWrite(*this);
		}
	}

	virtual ~ParamWriteVisitor() = 0;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_PARAM_VISITOR_HPP_
