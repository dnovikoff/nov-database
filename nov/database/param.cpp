#include "param.hpp"

namespace Nov {
namespace Database {

ParamImp::~ParamImp() {
}

Param::Param(const ParamImpPtr& p):imp(p) {
}

Param::Param() {
}

Param::~Param() {}

void Param::visitForRead(ParamReadVisitor& visitor) const {
	imp->visit(visitor);
}

void ParamRef::visitForWrite(ParamWriteVisitor& visitor) {
	imp->visit(visitor);
}

ParamWriteVisitor::~ParamWriteVisitor() {
}

ParamReadVisitor::~ParamReadVisitor() {
}

} // namespace Database
} // namespace Nov
