#include "stream.hpp"
#include "stream_imp.hpp"
#include "connection.hpp"
#include "result.hpp"
#include "statement.hpp"

namespace Nov {
namespace Database {

Stream::Stream(StreamImp* i):imp(i) {}

Stream::~Stream() {}

Stream::Stream(Stream&&) = default;

const std::string Stream::toString() const {
	return imp->out.str();
}

Stream& Stream::writeName(const std::string& name) {
	imp->writeField(name);
	return *this;
}

Stream& Stream::operator<<(const Param& p) {
	imp->writeValue(p);
	return *this;
}

Stream& Stream::operator<<(const InputParams& params) {
	bool isFirst = true;
	for (auto& p : params) {
		if (isFirst) {
			isFirst = false;
		} else {
			operator <<(", ");
		}
		operator <<(p);
	}
	return *this;
}

Stream& Stream::operator<<(const Fields& fields) {
	bool isFirst = true;
	for (auto& f : fields) {
		if (isFirst) {
			isFirst = false;
		} else {
			operator <<(", ");
		}
		operator <<(f);
	}
	return *this;
}

Stream& Stream::operator<<(const BindedFields& fields) {
	bool isFirst = true;
	for (auto& f : fields) {
		if (isFirst) {
			isFirst = false;
		} else {
			operator <<(", ");
		}
		operator <<(f.getField());
		bind(f.getParam());
	}
	return *this;
}

Stream& Stream::operator<<(const AssignList& p) {
	bool isFirst = true;
	for (auto& f : p.getAssignedFields()) {
		if (isFirst) {
			isFirst = false;
		} else {
			operator <<(p.getSeparator().c_str());
		}
		operator <<(f);
	}
	return *this;
}

Stream& Stream::operator<<(const ParamCopy& p) {
	return operator <<(static_cast<const Param&>(p));
}

Stream& Stream::operator<<(const Field& p) {
	return writeName(p.fieldName);
}

Stream& Stream::operator<<(const BindedField& p) {
	operator <<(p.getField());
	return bind(p.getParam());
}

Stream& Stream::bind(const ParamRef& param) {
	imp->outputParams->push_back(param);
	return *this;
}

Stream& Stream::operator<<(const AssignedField& p) {
	operator <<(p.getField()) << " = " << p.getParam();
	return *this;
}

Stream& Stream::operator<<(const char * m) {
	(imp->out) << m;
	return *this;
}


BindedResult Stream::execute() {
	auto st = imp->connection->newStatement(toString());

	BindedResult br;
	br.result = std::move(st->execute(imp->inputParams));
	br.outputParams = imp->outputParams;
	return br;
}

} // namespace Database
} // namespace Nov
