#ifndef NOV_DATABASE_STATEMENT_HPP_
#define NOV_DATABASE_STATEMENT_HPP_

#include <memory>
#include <string>

#include <nov/database/param.hpp>
#include <nov/database/param_visitor.hpp>

namespace Nov {
namespace Database {

class Result;

class Statement: public std::enable_shared_from_this<Statement> {
	friend class Result;
public:
	Result execute(const InputParams& input);
	Result execute();

	explicit Statement(const std::string& q);
	virtual ~Statement() = 0;
private:
	bool fetch(OutputParams& output);

	virtual void doPrepare(const std::string& query) = 0;
	virtual size_t doExecute(const InputParams& input) = 0;
	virtual bool doFetch(OutputParams& output) = 0;

	const std::string query;
	enum { INITIALIZED=1, PREPARED, EXECUTED } state = INITIALIZED;
	bool lastFetchResult = true;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_STATEMENT_HPP_
