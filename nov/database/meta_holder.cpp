#include "meta_holder.hpp"

namespace Nov {
namespace Database {

MetaHolder::MetaHolder() {
	metas_.reserve(64);
}

MetaHolder::~MetaHolder() {
}

MetaHolder::MetaHolder(MetaHolder&&) = default;
MetaHolder& MetaHolder::operator=(MetaHolder&&) = default;

void MetaHolder::initKeys() {
	apply([] (Meta& x) {
		x.initKeysIfNotInited();
	});
}

void MetaHolder::init() {
	apply([] (Meta& x) {
		x.initIfNotInited();
	});
}

void MetaHolder::apply(const Callback& func) {
	for (const auto& meta: metas_) {
		func(*meta);
	}
}

void MetaHolder::add(std::unique_ptr<Meta> meta) {
	metas_.push_back(std::move(meta));
}

} // namespace Database
} // namespace Nov
