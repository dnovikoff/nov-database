#include "stream_imp.hpp"

namespace Nov {
namespace Database {

StreamImp::StreamImp():outputParams(std::make_shared<OutputParams>()) {}

StreamImp::~StreamImp() {}

void StreamImp::writeField(const std::string& name) {
	out << "\"" << name << "\"";
}

void StreamImp::writePlaceholder() {
	out << "?";
}

void StreamImp::writeValue(const Param& p) {
	inputParams.push_back(p);
	writePlaceholder();
}

} // namespace Database
} // namespace Nov
