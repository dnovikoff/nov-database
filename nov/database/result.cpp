#include "result.hpp"
#include "statement.hpp"
#include "param.hpp"

namespace Nov {
namespace Database {

Result::Result() {
}

bool Result::fetch(OutputParams& output) {
	return statement->fetch(output);
}

bool BindedResult::fetch() {
	return result.fetch(*outputParams);
}

BindedResult::BindedResult() {
}

} // namespace Database
} // namespace Nov
