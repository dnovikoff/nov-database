#ifndef NOV_DATABASE_BACKEND_HPP_
#define NOV_DATABASE_BACKEND_HPP_

#include <memory>
#include <nov/database/configuration.hpp>

namespace Nov {
namespace Database {

class Backend {
	void* handle = nullptr;
public:
	std::shared_ptr<Configuration> createConfiguration(const std::string& connectString);

	explicit Backend(const std::string& backendName);
	~Backend();
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_BACKEND_HPP_
