#ifndef NOV_DATABASE_EXCEPTION_HPP_
#define NOV_DATABASE_EXCEPTION_HPP_

#include <stdexcept>

namespace Nov {
namespace Database {

class Exception: public std::runtime_error {
public:
	explicit Exception(const std::string& message): std::runtime_error(message) {}
};

void throwException(const std::string& message);

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_EXCEPTION_HPP_
