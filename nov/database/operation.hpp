#ifndef NOV_DATABASE_OPERATION_HPP_
#define NOV_DATABASE_OPERATION_HPP_

#include <memory>

#include <nov/database/connection.hpp>
#include <nov/database/meta.hpp>

namespace Nov {
namespace Database {
class BindedResult;

/**
 * Class for simple operations in their most simple forms: select, update, remove, insert
 */
class Operation {
public:
	typedef BindedFields Binded;
	typedef AssignedFields Assigned;

	Operation(const Meta& meta, const std::shared_ptr<Connection>& c);
	Operation(Operation&&) = default;

	const Field& getTable() const;

	BindedResult load(const Binded& output, const Assigned& where);
	bool exists(const Assigned& where);

	void insert(const Assigned& input);
	void insert(const Assigned& input, ParamRef id);
	size_t remove(const Assigned& where);
	size_t update(const Assigned& input, const Assigned& where);

private:
	const Meta& meta;
	const std::shared_ptr<Connection> connection;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_OPERATION_HPP_
