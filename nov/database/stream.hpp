#ifndef NOV_DATABASE_STREAM_HPP_
#define NOV_DATABASE_STREAM_HPP_

#include <memory>
#include <ostream>
#include <vector>

#include <nov/database/param.hpp>
#include <nov/database/field.hpp>

namespace Nov {
namespace Database {

class StreamImp;
class Result;
class BindedResult;

// Bridge
// Every database can have its own implementation for StreamImp
// The class is used to create correct queries, based on basic SQL syntax
// It quotes names, according to concrete database rules
// It also writes placeholders, according to concrete DB rules
// This class also hides the usage of Statement class
// and simplifies binding of input and output parameters

class Stream {
public:
	Stream& writeName(const std::string&);

	// Writes ParamCopy as a placeholder and add param to collection
	// The use of ParamCopy supports implicit construction from any type
	Stream& operator<<(const ParamCopy& p);

	// Support for other params. Ex. : base on ref on pointer
	Stream& operator<<(const Param& p);

	// Will add all params as input params and print them in form of placeholders like: ?, ?, ?
	Stream& operator<<(const InputParams& p);

	// Write table name or field name. Will quote the name correctly
	Stream& operator<<(const Field& p);

	// Will fields, separated by comma
	Stream& operator<<(const Fields& p);

	// Will fields, separated by comma with bind
	Stream& operator<<(const BindedFields& p);

	// Write assigned fields, separated by given separator.... EX.: "P1" = ? AND "P2" = ?
	Stream& operator<<(const AssignedField& p);

	// Write field name to stream and add parameter to output parameters list
	Stream& operator<<(const BindedField& p);

	// Write field name to stream and add parameter to output parameters list
	Stream& operator<<(const AssignList& p);

	// Write plain text to sql
	Stream& operator<<(const char*); // avoiding sql inject

	// Get resulting query
	const std::string toString() const;

	// Manual binding for output params. For cases like "SELECT count(1) FROM ..."
	Stream& bind(const ParamRef& param);

	Stream(Stream&&);
	~Stream();

	BindedResult execute();
private:
	friend class Connection;
	Stream(StreamImp* i);

	Stream(const Stream&) = delete;
	Stream& operator=(const Stream&) = delete;

	std::unique_ptr<StreamImp> imp;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_STREAM_HPP_
