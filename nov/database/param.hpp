#ifndef NOV_DATABASE_PARAM_HPP_
#define NOV_DATABASE_PARAM_HPP_

#include <memory>
#include <nov/database/param_visitor.hpp>
#include <nov/database/exception.hpp>

namespace Nov {
namespace Database {

class ParamImp {
public:
	virtual void visit(ParamReadVisitor& visitor) = 0;
	virtual void visit(ParamWriteVisitor& visitor) = 0;
	virtual ~ParamImp() = 0;
};

typedef std::shared_ptr<ParamImp> ParamImpPtr;

template<typename T>
class ParamRefImp final: public ParamImp {
	T* value;
public:
	explicit ParamRefImp(T* v):value(v) {
		static_assert(!std::is_pointer<T>::value, "ParamRefImp is not allowed to take pointer to pointer as argument");
	}

	void visit(ParamReadVisitor& visitor) override {
		visitor(value);
	}
	void visit(ParamWriteVisitor& visitor) override {
		if (value == nullptr) {
			throw Exception("You are not allowed to write in null values");
		}
		if(!visitor(*value)) {
			value = nullptr;
		}
	}

	bool isNull() const {
		return value == nullptr;
	}
};

template<typename T>
class ParamCopyImp final: public ParamImp {
	T value;
	ParamRefImp<T> refImp;
public:
	explicit ParamCopyImp(const T& v):value(v), refImp(&value) {}

	void visit(ParamReadVisitor& visitor) override {
		refImp.visit(visitor);
	}

	void visit(ParamWriteVisitor& visitor) override {
		refImp.visit(visitor);
		if (refImp.isNull()) {
			throw Exception("Null values are not supported for this param type");
		}
	}
};

// Bridge to param implementation
// Base type to be used in collection
// To construct it - use ParamCopy, ParamRef or ParamPointer classes
class Param {
protected:
	std::shared_ptr<ParamImp> imp;

	explicit Param(const ParamImpPtr& p);
public:
	Param();
	virtual ~Param();

	// Supports visitors to visi in read-only mode
	void visitForRead(ParamReadVisitor& visitor) const;
};

// Will copy passed value, so it is safe to use, when object created from is out of scope
class ParamCopy: public Param {
public:
	// Should declared copy constructors. Otherwise template constructor will be used to copy
	ParamCopy(const ParamCopy&) = default;
	ParamCopy(ParamCopy&&) = default;

	template<typename T>
	ParamCopy(const T& t):Param(std::make_shared<ParamCopyImp<T> >(t)) {
	}
};

// Having Param, created from a copy of value for write operation have little or no meaning
// so using of visitors that can modify value only supported for Params, passed by ref or pointer
// Main usage - is implicit constructing of ParamPointerBase or Param from value of supported type
class ParamRef: public Param {
public:
	// Should declared copy constructors. Otherwise template constructor will be used to copy
	ParamRef(const ParamRef&) = default;
	ParamRef(ParamRef& x):ParamRef(static_cast<const ParamRef&>(x)) {}
	ParamRef(ParamRef&&) = default;

	// try pointer first
	template<typename T>
	ParamRef(T* t):Param(std::make_shared<ParamRefImp<T> >(t)) {
	}

	template<typename T>
	ParamRef(T& t):ParamRef(&t) {
	}

	void visitForWrite(ParamWriteVisitor& visitor);
};

typedef std::vector<Param> InputParams;
typedef std::vector<ParamRef> OutputParams;

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_PARAM_HPP_
