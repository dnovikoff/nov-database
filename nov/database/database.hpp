#ifndef NOV_DATABASE_DATABASE_HPP_
#define NOV_DATABASE_DATABASE_HPP_

// all in one header

#include <nov/database/configuration.hpp>
#include <nov/database/connection.hpp>
#include <nov/database/result.hpp>
#include <nov/database/statement.hpp>

#include <nov/database/types/double.hpp>
#include <nov/database/types/id.hpp>
#include <nov/database/types/integer.hpp>
#include <nov/database/types/optional.hpp>
#include <nov/database/types/string.hpp>
#include <nov/database/types/timestamp.hpp>
#include <nov/database/types/binary.hpp>

#endif // NOV_DATABASE_DATABASE_HPP_
