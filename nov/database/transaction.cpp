#include "transaction.hpp"
#include "connection.hpp"
#include "exception.hpp"

namespace Nov {
namespace Database {

class Connection;

void TransactionScope::validateConnection() {
	if (!connection) {
		throw Exception("Transaction already closed");
	}
}

TransactionScope::TransactionScope(const std::shared_ptr<Connection>& c):connection(c) {
	c->startTransaction();
}

void TransactionScope::forceCommit() {
	validateConnection();
	connection->commitTransaction();
	connection.reset();
}

void TransactionScope::forceRollback() {
	validateConnection();
	connection->rollbackTransaction();
	connection.reset();
}

TransactionScope::~TransactionScope() {
	if (!connection) {
		return;
	}
	if (std::uncaught_exception()) {
		connection->rollbackTransaction();
	} else {
		connection->commitTransaction();
	}
}

} // namespace Database
} // namespace Nov
