#ifndef NOV_DATABASE_STREAM_IMP_HPP_
#define NOV_DATABASE_STREAM_IMP_HPP_

#include <memory>
#include <nov/database/param.hpp>
#include <nov/database/stream.hpp>

namespace Nov {
namespace Database {

class Connection;

class StreamImp {
	friend class Stream;
	friend class Connection;
public:
	StreamImp();
	virtual ~StreamImp();

	virtual void writeField(const std::string& name);
	virtual void writePlaceholder();
	void writeValue(const Param& p);

protected:
	std::ostringstream out;
	size_t getPlaceholderNumber() const { return inputParams.size(); }
private:
	InputParams inputParams;
	std::shared_ptr<OutputParams> outputParams;

	std::shared_ptr<Connection> connection;

	StreamImp(const StreamImp&) = delete;
	StreamImp& operator=(const StreamImp&) = delete;
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_STREAM_IMP_HPP_
