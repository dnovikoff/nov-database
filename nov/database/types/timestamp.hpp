#ifndef NOV_DATABASE_TYPES_TIMESTAMP_HPP_
#define NOV_DATABASE_TYPES_TIMESTAMP_HPP_

#include <boost/date_time/posix_time/posix_time.hpp>

namespace Nov {
namespace Database {
namespace Types {

typedef boost::posix_time::ptime Timestamp;

} // namespace Types
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_TIMESTAMP_HPP_
