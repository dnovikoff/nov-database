#ifndef NOV_DATABASE_TYPES_DOUBLE_HPP_
#define NOV_DATABASE_TYPES_DOUBLE_HPP_

namespace Nov {
namespace Database {
namespace Types {

typedef double Double;

} // namespace Types
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_DOUBLE_HPP_
