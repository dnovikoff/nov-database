#include <stdint.h>
#include <memory.h>

#include <list>

#include <iosfwd>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/write.hpp>

#include "binary.hpp"

namespace Nov {
namespace Database {
namespace Types {

uint8_t* BinaryData::createBuffer(size_t l) {
	auto x = std::shared_ptr<uint8_t>(new uint8_t[l], (std::default_delete<uint8_t[]>()));
	data = x;
	length = l;
	return x.get();
}

BinaryData::BinaryData(const size_t l, const std::shared_ptr<const void>& d):data(d), length(l) {
}

bool BinaryData::operator==(const BinaryData& rhs) const {
	if (&rhs == this) return true;
	if (getLength() != rhs.getLength()) return false;
	if (getLength() == 0) return true;
	if (data.get() == rhs.data.get()) return true;
	return memcmp(data.get(), rhs.data.get(), getLength()) == 0;
}

const std::string BinaryData::toString() const {
	if (isEmpty()) {
		return std::string();
	}
	const char * const cstr = as<char>();
	const bool endsWithZero = (cstr[length-1]==0);
	return std::string(cstr, length - (endsWithZero?1:0));
}

BinaryData BinaryData::copy() const {
	BinaryData tmp;

	if (length > 0) {
		memcpy(tmp.createBuffer(length), data.get(), length);
	}

	return tmp;
}

BinaryData BinaryData::ref(const size_t l, const void* d) {
	std::shared_ptr<const void> tmp(d, [](const void*) {} );
	return BinaryData(l, tmp);
}

Binary::~Binary() {}

bool Binary::operator==(const Binary& rhs) const {
	return data == rhs.data;
}

void SimpleBinary::decode(const BinaryData& x) {
	if (x.getLength() == 0u) {
		data = x;
	}
	data = x.copy();
}

const BinaryData SimpleBinary::encode() const {
	return data;
}

namespace {

class BufferChain {
	std::list<BinaryData> chain;
	size_t total = 0;

	BufferChain(const BufferChain&) = delete;
	BufferChain& operator=(const BufferChain&) = delete;
public:
	BufferChain() {}
	void push(const BinaryData& d) {
		total += d.getLength();
		chain.push_back(d);
	}

	BinaryData merge() const {
		if (chain.empty()) {
			return BinaryData();
		}
		if (chain.size() == 1u) {
			return chain.back();
		}
		BinaryData tmp;

		size_t pos = 0;
		uint8_t * writePtr = tmp.createBuffer(total);
		for(const auto& b : chain) {
			memcpy(&writePtr[pos], b.as<void>(), b.getLength());
			pos += b.getLength();
		}
		return tmp;
	}
};

class BufferWriter {
	BufferChain& chain;
public:
	explicit BufferWriter(BufferChain& c):chain(c) {}

	typedef char char_type;
	typedef boost::iostreams::sink_tag category;

	std::streamsize write(const char* s, std::streamsize n) {
		chain.push( BinaryData::ref(n, s).copy() );
		return n;
	}
};

namespace io = boost::iostreams;

template<typename Coder>
static BinaryData recode(const BinaryData& in, const Coder& coder) {
	if (in.isEmpty()) {
		return in;
	}

	BufferChain chain;
	{ // scope is important for correct operating
		io::filtering_ostream os;

		os.push(coder);
		os.push(BufferWriter(chain));

		os.write(in.as<char>(), std::streamsize(in.getLength()));
	}

	return chain.merge();
}

} // namespace

void ZipBinary::decode(const BinaryData& x) {
	data = recode(x, io::zlib_decompressor());
}

const BinaryData ZipBinary::encode() const {
	return recode(data, io::zlib_compressor(io::zlib::best_compression));
}

std::ostream& operator<<(std::ostream& out, const Binary& x) {
	out << x.getData();
	return out;
}

std::ostream& operator<<(std::ostream& out, const BinaryData& x) {
	out << x.getLength() << " bytes";
	return out;
}

} // namespace Types
} // namespace Database
} // namespace Nov
