#ifndef NOV_DATABASE_TYPES_STRING_HPP_
#define NOV_DATABASE_TYPES_STRING_HPP_

#include <string>

namespace Nov {
namespace Database {
namespace Types {

typedef std::string String;

} // namespace Types
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_STRING_HPP_
