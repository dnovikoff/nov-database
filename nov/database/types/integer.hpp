#ifndef NOV_DATABASE_TYPES_INTEGER_HPP_
#define NOV_DATABASE_TYPES_INTEGER_HPP_

#include <stdint.h>

namespace Nov {
namespace Database {
namespace Types {

typedef int64_t Integer;

} // namespace Types
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_INTEGER_HPP_
