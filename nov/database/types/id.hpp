#ifndef NOV_DATABASE_TYPES_ID_HPP_
#define NOV_DATABASE_TYPES_ID_HPP_

#include <ostream>
#include <nov/database/types/integer.hpp>
#include <nov/database/param.hpp>

namespace Nov {
namespace Database {
namespace Types {

template<typename T>
class Id {
	typedef Id<T> Self;
	friend ParamRefImp<Self>;
	friend class IdHacker;

	typedef Integer DataType;
	explicit Id(DataType value):data(value) {
	}
public:
	Id():data(0) {
	}
	bool isNull() const {
		return data == 0;
	}
	void print(std::ostream& out) const {
		out << "Id<" << T::className() << ">(";
		if (data == 0) {
			out << "NULL";
		} else {
			out << data;
		}
		out << ")";
	}
	bool operator==(const Self& rhs) const {
		return data == rhs.data;
	}
	bool operator<(const Self& rhs) const {
		return data < rhs.data;
	}
	bool operator!=(const Self& rhs) const {
		return !operator==(rhs);
	}
private:
	DataType data;
};

// Use only in case of emergency
class IdHacker {
public:
	template<typename T>
	static void setInteger(Id<T>& id, const Integer& x) {
		id.data = x;
	}

	template<typename T>
	static Integer getInteger(const Id<T>& id) {
		return id.data;
	}
};

template<typename T>
std::ostream& operator<<(std::ostream& out, const Id<T>& x) {
	x.print(out);
	return out;
}

} // namespace Types

template<typename T>
class ParamRefImp<Types::Id<T> >: public ParamImp {
	Types::Id<T>* value;
public:
	explicit ParamRefImp(Types::Id<T>* v):value(v) {}

	void visit(ParamReadVisitor& visitor) override {
		const Types::Integer* data = (value->data==0)?nullptr:&value->data;
		visitor(data);
	}

	void visit(ParamWriteVisitor& visitor) override {
		if (!visitor(value->data)) {
			value->data = 0;
		}
	}

	bool isNull() const {
		return !value || value->isNull();
	}
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_ID_HPP_
