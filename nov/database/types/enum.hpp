#ifndef NOV_DATABASE_TYPES_ENUM_HPP_
#define NOV_DATABASE_TYPES_ENUM_HPP_

#include <ostream>
#include <nov/database/types/integer.hpp>
#include <nov/database/param.hpp>
#include <nov/database/meta.hpp>

namespace Nov {
namespace Database {

template<typename T>
class EnumParamRefImp: public ParamImp {
	T& value;
public:
	explicit EnumParamRefImp(T* v):value(*v) {
		if (v == nullptr) {
			throw Exception("Read: Null values not allowed for EnumParamRefImp");
		}
		static_assert(!std::is_pointer<T>::value, "EnumParamRefImp is not allowed to take pointer to pointer as argument");
	}

	void visit(ParamReadVisitor& visitor) override {
		const Types::Integer data = static_cast<Types::Integer>(value);
		visitor(&data);
	}

	void visit(ParamWriteVisitor& visitor) override {
		Types::Integer tmp;
		if(!visitor(tmp)) {
			throw Exception("Write: Null values not allowed for EnumParamRefImp");
		}
		value = static_cast<T>(tmp);
	}

	bool isNull() const {
		return false;
	}
};

} // namespace Database
} // namespace Nov

#define NOV_DATABASE_ENUM_SUPPORT(X,DEFAULT)\
namespace Nov {\
namespace Database {\
template<> class ParamRefImp<X>: public EnumParamRefImp<X> { public: ParamRefImp(X* x):EnumParamRefImp(x) {} };\
template<> class ZeroValue<X> { public: static X value() { return X::DEFAULT; } };\
}}

#endif // NOV_DATABASE_TYPES_ENUM_HPP_
