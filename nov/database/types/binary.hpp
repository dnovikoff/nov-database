#ifndef NOV_DATABASE_TYPES_BINARY_HPP_
#define NOV_DATABASE_TYPES_BINARY_HPP_

#include <memory>
#include <string>

namespace Nov {
namespace Database {
namespace Types {

class BinaryData {
	// Construct info and pass free responsibility
	BinaryData(const size_t l, const std::shared_ptr<const void>& d);
public:
	BinaryData() {}

	size_t getLength() const {
		return length;
	}

	bool isEmpty() const {
		return length == 0;
	}

	template<typename T>
	const T* as() const {
		return static_cast<const T*>(data.get());
	}

	const std::string toString() const;

	// create copy of given data
	BinaryData copy() const;
	// Do not copy - just make ref
	static BinaryData ref(const size_t l, const void* d);

	// create buffer of given size and return writable ptr to it
	uint8_t* createBuffer(size_t l);

	bool operator==(const BinaryData& rhs) const;

	bool operator!=(const BinaryData& rhs) const {
		return !operator ==(rhs);
	}
private:
	// It is made shared_ptr to have ability to pass free function
	std::shared_ptr<const void> data;
	size_t length = 0;
};

class Binary {
public:
	const BinaryData& getData() const {
		return data;
	}

	void setData(const BinaryData& x) {
		data = x;
	}

	virtual ~Binary() = 0;

	/**
	 * Initialize from binary info.
	 * That info could be encoded, or compressed, or could be passed as is
	 */
	virtual void decode(const BinaryData& info) = 0;

	/**
	 * Return data to be saved. It could be compressed or encoded by concrete Binary Implementation
	 */
	virtual const BinaryData encode() const = 0;

	bool operator==(const Binary& rhs) const;

	bool operator!=(const Binary& rhs) const {
		return !operator ==(rhs);
	}
protected:
	BinaryData data;
};

/**
 * No compression or coding
 */
class SimpleBinary: public Binary {
public:
	void decode(const BinaryData& x) override;
	const BinaryData encode() const override;
};

/**
 * Will decode data from compressed state
 * and encode to compresseds
 */
class ZipBinary: public Binary {
public:
	void decode(const BinaryData& x) override;
	const BinaryData encode() const override;
};

std::ostream& operator<<(std::ostream& out, const BinaryData& b);
std::ostream& operator<<(std::ostream& out, const Binary& x);

} // namespace Types
} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_BINARY_HPP_
