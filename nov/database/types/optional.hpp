#ifndef NOV_DATABASE_TYPES_OPTIONAL_HPP_
#define NOV_DATABASE_TYPES_OPTIONAL_HPP_

#include <boost/optional.hpp>
#include <nov/database/param.hpp>

namespace Nov {
namespace Database {

template<typename T>
class ParamRefImp<boost::optional<T> >: public ParamImp {
	typedef boost::optional<T> Optional;
	Optional* value;
public:
	explicit ParamRefImp(Optional* v):value(v) {}

	void visit(ParamWriteVisitor& visitor) override {
		auto& opt = *value;
		T tmp;
		ParamRefImp<T> subImp(&tmp);
		subImp.visit(visitor);
		if (subImp.isNull()) {
			(Optional()).swap(opt); // Set null
		} else {
			opt = std::move(tmp);
		}
	}

	void visit(ParamReadVisitor& visitor) override {
		auto& opt = *value;

		ParamRefImp<T> subImp(opt?(&(*opt)):nullptr);
		subImp.visit(visitor);
	}

	bool isNull() const {
		return !value || !(*value);
	}
};

} // namespace Database
} // namespace Nov

#endif // NOV_DATABASE_TYPES_OPTIONAL_HPP_
