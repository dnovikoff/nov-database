#include "common_test.hpp"
#include "id_helpers.hpp"

#include <nov/database/backends/sqlite/configuration.hpp>
#include <nov/database/backends/sqlite/connection.hpp>
#include <nov/database/backends/sqlite/statement.hpp>

#include <nov/database/database.hpp>
#include <nov/database/types/optional.hpp>
#include <test/logger.hpp>

using namespace Nov::Database::Types;
using namespace Nov::Database;
using namespace Nov::Database::Sqlite;

template<typename T>
static bool check(const char * query, const T& expects) {
	Test::LoggerScope logger;
	ConfigurationTemp conf;
	auto con = conf.newConnection();
	auto st = con->newStatement(query);
	Result r = st->execute();
	OutputParams out;
	T tmp;
	out.push_back(tmp);
	if (!r.fetch(out)) {
		std::cout << "Expects value to be fetched" << std::endl;
		return false;
	}
	if (tmp != expects) {
		std::cout << "Expected " << expects << " but fetched " << tmp << std::endl;
		return false;
	}
	if (r.fetch(out)) {
		std::cout << "Expected only one value to be fetched" << std::endl;
		return false;
	}
	return true;
}

BOOST_AUTO_TEST_CASE ( select1integer ) {
	BOOST_CHECK(check<Integer>("SELECT 1", 1));
}

BOOST_AUTO_TEST_CASE ( select1double ) {
	BOOST_CHECK(check<Double>("SELECT 4.9", 4.9));
}

BOOST_AUTO_TEST_CASE ( select1string ) {
	BOOST_CHECK(check<String>("SELECT 'iam a string'", "iam a string"));
}

BOOST_AUTO_TEST_CASE ( select1id ) {
	BOOST_CHECK(check("SELECT 123", createAnyId(123)));
}

BOOST_AUTO_TEST_CASE ( select1nullid ) {
	BOOST_CHECK(check("SELECT NULL", createNull()));
}

BOOST_AUTO_TEST_CASE ( selectNull ) {
	BOOST_CHECK(check("SELECT NULL", boost::optional<Integer>()));
}

BOOST_AUTO_TEST_CASE ( selectOptional ) {
	BOOST_CHECK(check("SELECT 456", boost::optional<Integer>(456)));
}
