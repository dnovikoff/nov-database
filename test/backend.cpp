#include "common_test.hpp"

#include <memory>

#include <nov/database/database.hpp>
#include <nov/database/configuration.hpp>
#include <nov/database/backend.hpp>

#include <nov/database/param.hpp>
#include <nov/database/meta.hpp>
#include <nov/database/result.hpp>
#include <nov/database/stream.hpp>
#include <nov/database/operation.hpp>
#include <nov/database/meta_holder.hpp>

#include <nov/database/transaction.hpp>

#include <test/logger.hpp>
#include "enum.hpp"

using namespace Nov::Database::Types;
using namespace Nov::Database;

class TestMeta: public Meta {
public:
	NOV_DB_FIELD(MyId);
	NOV_DB_FIELD(MyName);
	NOV_DB_FIELD(MyDouble);
	NOV_DB_FIELD(MyTime);
	NOV_DB_FIELD(MyBinary);
	NOV_DB_FIELD(MyZipBinary);
	NOV_DB_FIELD(MyEnum);

	static void setInstance(const TestMeta* p) {
		instance_ = p;
	}
	static const TestMeta* instance() {
		return instance_;
	}
private:
	friend class Nov::Database::MetaHolder;

	TestMeta(): Meta("TestTable") {
	}

	TestMeta(TestMeta&&) = default;

	void init() override {
		addField<Integer>(MyId);
		addField<String>(MyName);
		addField<Double>(MyDouble);
		addField<Timestamp>(MyTime);
		addField<SimpleBinary>(MyBinary);
		addField<ZipBinary>(MyZipBinary);
		addField<TestEnum>(MyEnum);

		PrimaryKey key;
		key.addField(MyId);
		addKey(key);
		setSerial(MyId);
	}

	void initKeys() override {
	}

	static const TestMeta* instance_;
};

class Test2Meta: public Meta {
public:
	NOV_DB_FIELD(MyId);
	NOV_DB_FIELD(MyForeign);

	Test2Meta(): Meta("TestForeignTable") {
	}

private:
	void init() override {
		addField<Integer>(MyId);
		addField<Integer>(MyForeign);

		PrimaryKey key;
		key.addField(MyId);
		addKey(key);
		setSerial(MyId);
	}

	void initKeys() override {
		IndexKey fk;
		fk.addField(MyForeign);
		addKey(fk);
		addForeignKey(fk, *TestMeta::instance());
	}
};

const TestMeta* TestMeta::instance_ = nullptr;

class TestObj {
public:
	static const TestMeta& meta() {
		return *TestMeta::instance();
	}
	TestObj() {
		MyTime = boost::posix_time::from_time_t(time(NULL));
	}

	typedef Id<TestObj> IdType;
	constexpr static const char * className() { return "Test"; }

	IdType MyId;
	String MyName;
	Double MyDouble = 0;
	Timestamp MyTime;
	SimpleBinary MyBinary;
	ZipBinary MyZipBinary;
	TestEnum MyEnum = TestEnum::Zero;
};

class Conf {
public:
#ifndef NOV_TEST_PGSQL
	Conf(): be("sqlite") {
		conf = be.createConfiguration("nov_test");
	}
#else
	Conf(): be("pgsql") {
		conf = be.createConfiguration("user=nov_test password=nov_test");
	}
#endif
	std::shared_ptr<Connection> newConnection() {
		return conf->newConnection();
	}

private:
	Test::LoggerScope logger_;
	Backend be;
	std::shared_ptr<Configuration> conf;
};

class NoTablesTester {
public:
	NoTablesTester() {
		conn = conf.newConnection();
	}
	Connection& connection() {
		return *conn;
	}
	const std::shared_ptr<Connection>& connectionPtr() {
		return conn;
	}

	void dropTableByMeta(const Meta& m) {
		Stream q = connection().createStream();
		q << "DROP TABLE IF EXISTS " << m.getTable()
#ifdef NOV_TEST_PGSQL
			<< " CASCADE"
#endif
		;
		q.execute();
	}

	virtual ~NoTablesTester() {
	}

private:
	Conf conf;
	std::shared_ptr<Connection> conn;
};

class Tester: public NoTablesTester {
public:
	const TestMeta& meta() {
		return *TestMeta::instance();
	}

	Tester() {
		holder_.add<TestMeta>();
		holder_.init();

		createTable();
	}

	void dropTable() {
		dropTableByMeta(meta());
	}

	void createTable() {
		dropTable();
		connection().createTable(meta());
	}

	TestObj::IdType insert(TestObj& obj) {
#define TMP(X) meta().X.assignRef(obj.X)
		AssignedFields assigned { TMP(MyName), TMP(MyDouble), TMP(MyTime), TMP(MyBinary), TMP(MyZipBinary), TMP(MyEnum) };
#undef TMP
		if (obj.MyId.isNull() ) {
			connection().insert(meta(), assigned, ParamRef(obj.MyId));
		} else {
			connection().insert(meta(), assigned);
		}
		return obj.MyId;
	}

	TestObj::IdType insertAny() {
		TestObj tmp;
		tmp.MyTime = boost::posix_time::from_time_t(time(NULL));
		insert(tmp);
		return tmp.MyId;
	}

	bool checkId(TestObj::IdType id) {
		Operation o(meta(), connectionPtr());
		return o.exists({TestObj::meta().MyId.assignRef(id)});
	}

	MetaHolder holder_;
};

BOOST_AUTO_TEST_CASE ( insert ) {
	Tester t;
	TestObj obj1;
	TestObj obj2;

	obj1.MyName = "hello";
	obj2.MyName = "world";
	obj1.MyTime = boost::posix_time::from_time_t(time(NULL));
	obj2.MyTime = obj1.MyTime;

	t.insert(obj1);
	t.insert(obj2);

	String name;

	Stream s =  t.connection().createStream();

	TestObj::IdType outId;

	const auto& meta = *TestMeta::instance();

	s << "SELECT " << meta.MyId.bind(outId) << " FROM " << meta.getTable() << " WHERE " << meta.MyName.assignRef(name);

	BindedResult r = s.execute();

	BOOST_CHECK( !r.fetch() );

	name = "world";
	r = s.execute();
	BOOST_CHECK( r.fetch() );
	BOOST_CHECK_EQUAL(outId, obj2.MyId);
	BOOST_CHECK( !r.fetch() );
}

BOOST_AUTO_TEST_CASE ( insertAuto ) {
	Tester t;
	const auto id1 = t.insertAny();
	const auto id2 = t.insertAny();
	BOOST_CHECK_NE(id1, id2);
}

BOOST_AUTO_TEST_CASE ( selectCount ) {
	Tester t;
	Integer count = 0;
	Stream s =  t.connection().createStream();
	s << "SELECT count(1) FROM " << TestMeta::instance()->getTable();
	s.bind(count);

	BOOST_REQUIRE( s.execute().fetch() );
	BOOST_CHECK_EQUAL(count, 0);

	t.insertAny();
	BOOST_REQUIRE( s.execute().fetch() );
	BOOST_CHECK_EQUAL(count, 1);

	t.insertAny();
	BOOST_REQUIRE( s.execute().fetch() );
	BOOST_CHECK_EQUAL(count, 2);
}

BOOST_AUTO_TEST_CASE ( operationInsert ) {
	Tester tester;
	Operation o(tester.meta(), tester.connectionPtr());

	TestObj t;
	const TestMeta& meta = t.meta();
	t.MyDouble = 0.1;
	t.MyName = "name";

	const AssignedFields fields { meta.MyDouble.assignRef(t.MyDouble), meta.MyName.assignRef(t.MyName) };

	o.insert(fields, t.MyId);
}

BOOST_AUTO_TEST_CASE ( operationRemove ) {
	Tester tester;
	auto i1 = tester.insertAny();
	auto i2 = tester.insertAny();

	Operation o(tester.meta(), tester.connectionPtr());

	BOOST_CHECK_EQUAL(o.remove({tester.meta().MyId.assignRef(i2)}), 1u);
	BOOST_CHECK_EQUAL(o.remove({tester.meta().MyId.assignRef(i2)}), 0u);
	BOOST_CHECK_EQUAL(o.remove({tester.meta().MyId.assignRef(i1)}), 1u);
}

BOOST_AUTO_TEST_CASE ( operationUpdate ) {
	Tester tester;
	auto i1 = tester.insertAny();
	auto i2 = tester.insertAny();

	Operation o(tester.meta(), tester.connectionPtr());
	o.remove({tester.meta().MyId.assignRef(i2)});

	TestObj t;
	t.MyName = "hello";
	t.MyDouble = 1.0;
	BOOST_CHECK_EQUAL(1u, o.update({tester.meta().MyName.assignRef(t.MyName), tester.meta().MyDouble.assignRef(t.MyDouble)}, {tester.meta().MyId.assignRef(i1)}));
	BOOST_CHECK_EQUAL(0u, o.update({tester.meta().MyName.assignRef(t.MyName), tester.meta().MyDouble.assignRef(t.MyDouble)}, {tester.meta().MyId.assignRef(i2)}));
}

BOOST_AUTO_TEST_CASE ( operationLoad ) {
	Tester tester;
	TestObj t1;
	t1.MyName = "hello";
	t1.MyDouble = 1;
	tester.insert(t1);

	TestObj t2;
	t2.MyName = "world";
	t2.MyDouble = 1;
	tester.insert(t2);

	Operation o(tester.meta(), tester.connectionPtr());

	TestObj out;
	const BindedFields output = {out.meta().MyName.bind(out.MyName), out.meta().MyDouble.bind(out.MyDouble)};
	auto r = o.load(output, {out.meta().MyName.assignRef(t1.MyName)});

	BOOST_REQUIRE(r.fetch());

	BOOST_CHECK_EQUAL(out.MyName, "hello");
	BOOST_CHECK_EQUAL(out.MyDouble, 1);

	BOOST_REQUIRE(!r.fetch());
}

BOOST_AUTO_TEST_CASE ( binary ) {
	const std::string str("some binary");
	Tester tester;
	TestObj t1;
	t1.MyBinary.setData(Types::BinaryData::ref(str.length(), str.c_str()));
	tester.insert(t1);

	Operation o(tester.meta(), tester.connectionPtr());
	TestObj out;
	const BindedFields output = {out.meta().MyBinary.bind(out.MyBinary)};
	auto r = o.load(output, {out.meta().MyId.assignRef(t1.MyId)});
	BOOST_REQUIRE(r.fetch());
	BOOST_CHECK_EQUAL(str, out.MyBinary.getData().toString());
}

BOOST_AUTO_TEST_CASE ( emptyWhereGeneration ) {
	Tester tester;
	Operation o(tester.meta(), tester.connectionPtr());
	TestObj out;
	auto r = o.load({out.meta().MyName.bind(out.MyName)}, {});
	BOOST_REQUIRE(!r.fetch());
}

BOOST_AUTO_TEST_CASE ( zipBinary ) {
	const std::string str("Some string to compress");
	Tester tester;
	TestObj t1;
	t1.MyZipBinary.setData(Types::BinaryData::ref(str.length(), str.c_str()));
	tester.insert(t1);

	Operation o(tester.meta(), tester.connectionPtr());
	TestObj out;
	const BindedFields output = {out.meta().MyZipBinary.bind(out.MyZipBinary)};
	auto r = o.load(output, {out.meta().MyId.assignRef(t1.MyId)});
	BOOST_REQUIRE(r.fetch());
	BOOST_CHECK_EQUAL(str, out.MyZipBinary.getData().toString());
}

BOOST_AUTO_TEST_CASE ( createForeignKey ) {
	Tester tester;
	tester.createTable();
	Connection& c = tester.connection();

	Test2Meta meta2;
	meta2.initIfNotInited();
	tester.dropTableByMeta(meta2);

	c.createTable(meta2);
	c.createKeys(meta2);
}

BOOST_AUTO_TEST_CASE ( createIndex ) {
	Tester tester;
	tester.createTable();
	Connection& c = tester.connection();

	Test2Meta meta2;
	meta2.initIfNotInited();
	tester.dropTableByMeta(meta2);

	c.createTable(meta2);
	c.createKeys(meta2);
}

BOOST_AUTO_TEST_CASE ( beginCommitSyntax ) {
	Tester tester;
	Connection& c = tester.connection();
	c.startTransaction();
	c.commitTransaction();
}

BOOST_AUTO_TEST_CASE ( beginRollbackSyntax ) {
	Tester tester;
	Connection& c = tester.connection();
	c.startTransaction();
	c.rollbackTransaction();
}

BOOST_AUTO_TEST_CASE ( transactionScope ) {
	Tester tester;
	auto c = tester.connectionPtr();
	TestObj::IdType id;

	{
		TransactionScope trans(c);
		id = tester.insertAny();
		BOOST_CHECK(tester.checkId(id));
	}
	BOOST_CHECK(tester.checkId(id));
}

namespace {

struct TestEx {};

} // namespace

BOOST_AUTO_TEST_CASE ( transactionScopeException ) {
	Tester tester;
	auto c = tester.connectionPtr();
	TestObj::IdType id;

	try {
		TransactionScope trans(c);
		id = tester.insertAny();
		BOOST_CHECK(tester.checkId(id));
		throw TestEx(); // Rollback at throw
	} catch(const TestEx&) {
	}
	BOOST_CHECK(!tester.checkId(id));
}

BOOST_AUTO_TEST_CASE ( transactionForceCommit ) {
	Tester tester;
	auto c = tester.connectionPtr();
	TestObj::IdType id;

	try {
		TransactionScope trans(c);
		id = tester.insertAny();
		BOOST_CHECK(tester.checkId(id));
		trans.forceCommit();
		throw TestEx(); // Rollback at throw
	} catch(const TestEx&) {
	}
	// Commited before excpetion
	BOOST_CHECK(tester.checkId(id));
}

BOOST_AUTO_TEST_CASE ( transactionForceRollaback ) {
	Tester tester;
	auto c = tester.connectionPtr();
	TestObj::IdType id;

	{
		TransactionScope trans(c);
		id = tester.insertAny();
		BOOST_CHECK(tester.checkId(id));
		trans.forceRollback();
	}
	// Rollbacked before out if scope
	BOOST_CHECK(!tester.checkId(id));
}

BOOST_AUTO_TEST_CASE ( enumTest ) {
	Tester tester;
	Operation o(tester.meta(), tester.connectionPtr());

	TestObj t;
	const TestMeta& meta = t.meta();
	t.MyEnum = TestEnum::Second;

	tester.insert(t);

	auto s =  tester.connection().createStream();

	TestEnum out = TestEnum::First;

	s << "SELECT " << meta.MyEnum.bind(out) << " FROM " << meta.getTable() << " WHERE " << meta.MyId.assignRef(t.MyId);

	BOOST_REQUIRE(s.execute().fetch());
	BOOST_CHECK_EQUAL(out, TestEnum::Second);
}

BOOST_AUTO_TEST_CASE ( updateReturning ) {
	Tester tester;

	TestObj t;
	const TestMeta& meta = t.meta();

	t.MyName = "Tester";
	t.MyDouble = 1.0;
	tester.insert(t);
	auto id1 = t.MyId;
	t.MyName = "TheOther";
	t.MyDouble = 2.0;
	tester.insert(t);

	auto& c = tester.connection();
	auto s = c.createStream();

	s << "UPDATE " << meta.getTable() << " SET " << meta.MyName.assignCopy(String("Tester1")) << " WHERE " << meta.MyDouble.assignCopy(1.0);

	BindedFields binded {meta.MyName.bind(t.MyName), meta.MyId.bind(t.MyId)};
	BOOST_REQUIRE(c.executeUpdateReturning(s, meta, binded));

	BOOST_CHECK_EQUAL(t.MyId, id1);
	BOOST_CHECK_EQUAL(t.MyName, "Tester1");
}
