#ifndef TEST_TEST_BACKEND_CONNECTION_HPP_
#define TEST_TEST_BACKEND_CONNECTION_HPP_

#include <nov/database/backends/sqlite/connection.hpp>

namespace Test {
using namespace Nov::Database;

class ConnectionImp: public Sqlite::ConnectionImp {
	typedef Sqlite::ConnectionImp ParentType;
public:
	ConnectionImp();
	std::shared_ptr<Statement> newStatement(const std::string& query) override;
};

} // namespace Test

#endif // TEST_TEST_BACKEND_CONNECTION_HPP_
