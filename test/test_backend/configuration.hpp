#ifndef TEST_TEST_BACKEND_CONFIGURATION_HPP_
#define TEST_TEST_BACKEND_CONFIGURATION_HPP_

#include <string>

#include <nov/database/configuration.hpp>

#include <test/logger.hpp>

namespace Test {
using namespace Nov::Database;

class ConfigurationImp: public Configuration {
public:
	std::shared_ptr<Connection> newConnection() override;

private:
	LoggerScope logger_;
};

} // namespace Test

#endif // TEST_TEST_BACKEND_CONFIGURATION_HPP_
