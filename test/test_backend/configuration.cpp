#include "configuration.hpp"
#include "connection.hpp"

namespace Test {

std::shared_ptr<Connection> ConfigurationImp::newConnection() {
	return std::make_shared<ConnectionImp>();
}

} // namespace Test
