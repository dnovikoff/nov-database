#include "connection.hpp"
#include "statement.hpp"

namespace Test {

ConnectionImp::ConnectionImp():ParentType(":memory:") {
}

std::shared_ptr<Statement> ConnectionImp::newStatement(const std::string& query) {
	return std::make_shared<StatementImp>(query);
}

} // namespace Test
