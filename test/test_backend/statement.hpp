#ifndef TEST_TEST_BACKEND_STATEMENT_HPP_
#define TEST_TEST_BACKEND_STATEMENT_HPP_

#include <nov/database/statement.hpp>

namespace Test {
using namespace Nov::Database;

class StatementImp: public Statement {
	void doPrepare(const std::string& query) override;
	size_t doExecute(const InputParams& input) override;
	bool doFetch(OutputParams& output) override;

	static std::string latestQuery;
	static std::string firstQuery;
public:
	static void reset();
	static const std::string& getLatestQuery() {
		return latestQuery;
	}
	static const std::string& getFirstQuery() {
		return firstQuery;
	}
	explicit StatementImp(const std::string& query);
};

} // namespace Test

#endif // TEST_TEST_BACKEND_STATEMENT_HPP_
