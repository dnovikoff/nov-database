#include "statement.hpp"

namespace Test {

StatementImp::StatementImp(const std::string& query):Statement(query) {}

std::string StatementImp::latestQuery;
std::string StatementImp::firstQuery;

void StatementImp::doPrepare(const std::string& query) {
	latestQuery = query;
	if (firstQuery.empty()) {
		firstQuery = query;
	}
}

void StatementImp::reset() {
	firstQuery.clear();
	latestQuery.clear();
}

size_t StatementImp::doExecute(const InputParams&) { return 0; }


bool StatementImp::doFetch(OutputParams&) {
	return false;
}

} // namespace Test
