#ifndef TEST_ID_HELPERS_HPP_
#define TEST_ID_HELPERS_HPP_

#include <nov/database/types/id.hpp>
#include <nov/database/param.hpp>

namespace {

using namespace Nov::Database::Types;
using namespace Nov::Database;

#define TEST_CLASS(X) class X { public: constexpr static const char * className() { return #X; } }

TEST_CLASS(T1);
TEST_CLASS(T2);

template<typename T=T1>
Id<T> createId(const Integer& id) {
	Id<T> ret;
	IdHacker::setInteger(ret, id);
	return ret;
}

template<typename T=T1>
Id<T> createNull() {
	return Id<T>();
}

class Any {
public:
	constexpr static const char * className() { return "TestClass"; }
};

Id<Any> createAnyId(const Integer& val) {
	return createId<Any>(val);
}

} // namespace

#endif // TEST_ID_HELPERS_HPP_
