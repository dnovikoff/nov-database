#ifndef TEST_ENUM_HPP_
#define TEST_ENUM_HPP_

#include <nov/database/types/enum.hpp>

enum class TestEnum {
	Zero, First, Second,
};

NOV_DATABASE_ENUM_SUPPORT(TestEnum, Zero)

inline std::ostream& operator<<(std::ostream& out, TestEnum e) {
	return out << "TestEnum::" << static_cast<int>(e);
}

#endif // TEST_ENUM_HPP_
