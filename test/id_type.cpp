#include "common_test.hpp"
#include "id_helpers.hpp"


BOOST_AUTO_TEST_CASE ( defaultIsNull ) {
	BOOST_CHECK (createNull().isNull());
}

BOOST_AUTO_TEST_CASE ( notDefaultIsNotNull ) {
	BOOST_CHECK (!createId(1u).isNull());
}

BOOST_AUTO_TEST_CASE ( testEquals ) {
	BOOST_CHECK_EQUAL (createId(1u) ,createId(1u));
}

BOOST_AUTO_TEST_CASE ( testNotEquals ) {
	BOOST_CHECK_NE (createId(1u) ,createId(2u));
}

BOOST_AUTO_TEST_CASE ( testLess ) {
	BOOST_CHECK(createId(1u) < createId(2u));
}

BOOST_AUTO_TEST_CASE ( testNotLess1 ) {
	BOOST_CHECK(!(createId(1u) < createId(1u)));
}

BOOST_AUTO_TEST_CASE ( testNotLess2 ) {
	BOOST_CHECK(!(createId(2u) < createId(1u)));
}
