#ifndef TEST_LOGGER_HPP_
#define TEST_LOGGER_HPP_

#include <string>

#include <nov/log/default_logger.hpp>
#include <nov/log/cout_logger.hpp>

#include <nov/database/logger.hpp>

namespace Test {
using namespace Nov::Database;

class LoggerScope {
public:
	LoggerScope(): logger_(backend_, "DBTest") {
		logger_.setThreshold(Nov::Log::LogLevel::WARNING);
		setLogger(&logger_);
	}

	~LoggerScope() {
		setLogger(nullptr);
	}

private:
	Nov::Log::CoutLogger backend_;
	Nov::Log::DefaultLogger logger_;
};

} // namespace Test

#endif // TEST_LOGGER_HPP_
