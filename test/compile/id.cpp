#include "id.hpp"

void foo() {
	Id<class My> id; // Default constructor
	Id<class My> copy(id); // Copy constructor from same id
	id = copy; // copy assign enabled

	// Operators
	get1() == get1();
	get1() != get1();
	get1() < get1();
}
