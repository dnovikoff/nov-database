#include "id.hpp"

void foo() {
	Id<class SomeType> some;
	Id<class OtherType> other(some); // Not allowed to construct from other ids
}
