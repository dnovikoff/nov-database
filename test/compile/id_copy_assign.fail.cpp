#include "id.hpp"

void foo() {
	Id<class SomeType> some;
	Id<class OtherType> other;

	some = other; // Not allowed to assign from other ids
}
