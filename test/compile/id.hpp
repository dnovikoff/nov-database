#ifndef TEST_COMPILE_ID_HPP_
#define TEST_COMPILE_ID_HPP_

#include <iostream>
#include <nov/database/types/id.hpp>

using namespace Nov::Database::Types;

namespace {
class T1;
class T2;

template<typename T>
Id<T> getId() { return Id<T>(); }

Id<T1> get1() { return getId<T1>(); }
Id<T2> get2() { return getId<T2>(); }

} // namespace

#endif // TEST_COMPILE_ID_HPP_
