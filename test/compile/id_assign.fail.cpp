#include "id.hpp"

void foo() {
	Id<class AnyType> id;
	id = 1u;
	// Not allowed to assign an integer
}
