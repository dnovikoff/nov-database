#include "id.hpp"

void foo() {
	Id<class AnyType> id(1u); // Not allowed to construct from integers
}
