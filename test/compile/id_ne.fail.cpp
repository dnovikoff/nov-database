#include "id.hpp"

void foo() {
	Id<class SomeType> some;
	Id<class OtherType> other;

	other != some; // Not allowed to compare different ids
}
