#include <memory>
#include "common_test.hpp"

#include <nov/database/database.hpp>
#include <nov/database/backend.hpp>

#include <test/logger.hpp>

using namespace Nov::Database;

BOOST_AUTO_TEST_CASE ( correctName ) {
	Test::LoggerScope logger;
	Backend be("sqlite");

	auto p = be.createConfiguration("hello");
	BOOST_CHECK( p );

	auto conn = p->newConnection();
	BOOST_CHECK( conn );

	BOOST_CHECK_EQUAL(conn->execute("SELECT 1").getAffacted(), 0u);
}

BOOST_AUTO_TEST_CASE ( incorrectName ) {
	BOOST_CHECK_THROW( Backend be("unknown_backend_name"), std::runtime_error);
}
