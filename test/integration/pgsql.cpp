#define NOV_TEST_PGSQL
#include "../backend.cpp"

// SOME Postgres-Only tests

namespace {

template<typename T>
struct Cast {
};

#define TMP(TYPE, STR) \
template<>\
struct Cast<TYPE> {\
	static constexpr const char * value = #STR;\
}

TMP(Timestamp, timestamptz);
TMP(Integer, bigint);
TMP(String, varchar);
TMP(Double, double precision);
#undef TMP

template<typename ToType, typename FromType>
class PgCast {
public:
	template<typename T, typename F>
	T readBase(F x) {
		Stream s = t.connection().createStream();
		s << "SELECT " << x << "::" << Cast<T>::value;
		T value;
		s.bind(value);
		s.execute().fetch();
		return value;
	}

	ToType read(FromType x) {
		return readBase<ToType, FromType>(x);
	}

	FromType read2(ToType x) {
		return readBase<FromType, ToType>(x);
	}

 private:
	NoTablesTester t;
};

} // namespace

// Casts. We will check that data in the db is as expected

BOOST_AUTO_TEST_CASE ( intToString ) {
	PgCast<String, Integer> c;
	BOOST_CHECK_EQUAL(c.read(0), "0");
	BOOST_CHECK_EQUAL(c.read(10), "10");
	BOOST_CHECK_EQUAL(c.read(99), "99");
}

BOOST_AUTO_TEST_CASE ( stringToInt ) {
	PgCast<Integer, String> c;
	BOOST_CHECK_EQUAL(c.read("0"), 0);
	BOOST_CHECK_EQUAL(c.read("10"), 10);
	BOOST_CHECK_EQUAL(c.read("99"), 99);
}

BOOST_AUTO_TEST_CASE ( doubleToString ) {
	PgCast<String, Double> c;
	BOOST_CHECK_EQUAL(c.read(0), "0");
	BOOST_CHECK_EQUAL(c.read(12.345), "12.345");
	BOOST_CHECK_EQUAL(c.read(99.9), "99.9");
}

BOOST_AUTO_TEST_CASE ( stringToDouble ) {
	PgCast<Double, String> c;
	BOOST_CHECK_EQUAL(c.read("0"), 0);
	BOOST_CHECK_EQUAL(c.read("12.345"), 12.345);
	BOOST_CHECK_EQUAL(c.read("99.9"), 99.9);
}

static boost::posix_time::ptime createDate(int y, int m, int d) {
	boost::posix_time::ptime p(boost::gregorian::date(y,m,d));
	return p;
}

BOOST_AUTO_TEST_CASE ( timeToString ) {
	PgCast<Timestamp, String> s2t;
	const Timestamp ts(createDate(2014,10,15));
	const auto str = s2t.read2(ts);
	const auto ts2 = s2t.read(str);

	BOOST_CHECK_EQUAL(ts, ts2);
}
