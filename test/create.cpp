#include "common_test.hpp"

#include <memory>

#include "test_backend/configuration.hpp"
#include "test_backend/statement.hpp"
#include <nov/database/connection.hpp>

#include <nov/database/exception.hpp>

#include <nov/database/types/integer.hpp>
#include <nov/database/types/double.hpp>
#include <nov/database/types/string.hpp>
#include <nov/database/types/timestamp.hpp>
#include <nov/database/param.hpp>
#include <nov/database/meta.hpp>

using namespace Nov::Database::Types;
using namespace Nov::Database;

class TestCreate {
public:
	explicit TestCreate(const std::string& tableName): meta(tableName) {
		connection = conf.newConnection();
	}
	template<typename T>
	void add(const std::string& name) {
		meta.addField<T>(name);
	}
	void addKey(const Key& k) {
		meta.addKey(k);
	}
	void setSerial(const std::string& name) {
		meta.setSerial(Field(name));
	}
	const std::string& query() const {
		Test::StatementImp::reset();
		connection->createTable(meta);
		return Test::StatementImp::getFirstQuery();
	}

private:
	class TestMeta: public Meta {
		void init() override {}
		void initKeys() override {}
	public:
		explicit TestMeta(const std::string& name):Meta(name) {}
		using Meta::addKey;
		using Meta::setSerial;
		template<typename T>
		void addField(const std::string& name) {
			Meta::addField<T>(Field(name));
		}
	};

	Test::ConfigurationImp conf;
	std::shared_ptr<Connection> connection;
	TestMeta meta;
};

class TestKey: public Key {
public:
	TestKey(Type t):Key(t) {}

	void addField(const std::string& name) {
		Key::addField(Field(name));
	}
};

BOOST_AUTO_TEST_CASE ( oneField ) {
	TestCreate test("Hello");
	test.add<Integer>("First");

	TestKey key(Key::Primary);
	key.addField("First");
	test.addKey(key);

	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "Hello" ("First" INTEGER, PRIMARY KEY("First")))");
}

BOOST_AUTO_TEST_CASE ( twoFields ) {
	TestCreate test("T");
	test.add<Integer>("a");
	test.add<String>("b");

	TestKey key(Key::Primary);
	key.addField("a");
	test.addKey(key);

	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "T" ("a" INTEGER, "b" TEXT, PRIMARY KEY("a")))");
}

BOOST_AUTO_TEST_CASE ( twoPrimaryFields ) {
	TestCreate test("T");
	test.add<Integer>("a");
	test.add<String>("b");

	TestKey key(Key::Primary);
	key.addField("a");
	key.addField("b");
	test.addKey(key);

	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "T" ("a" INTEGER, "b" TEXT, PRIMARY KEY("a", "b")))");
}

BOOST_AUTO_TEST_CASE ( oneSerial ) {
	TestCreate test("T");
	test.add<Integer>("a");
	test.setSerial("a");

	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "T" ("a" INTEGER PRIMARY KEY AUTOINCREMENT))");
}

BOOST_AUTO_TEST_CASE ( oneSerialIgnoresPK ) {
	TestCreate test("T");
	test.add<Integer>("a");
	test.setSerial("a");

	TestKey key(Key::Primary);
	key.addField("a");
	test.addKey(key);

	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "T" ("a" INTEGER PRIMARY KEY AUTOINCREMENT))");
}

//BOOST_AUTO_TEST_CASE ( serialTransformsMultiPKtoUnique ) {
//	TestCreate test("T");
//	test.add<Integer>("a");
//	test.add<Integer>("b");
//	test.setSerial("a");
//
//	TestKey key(Key::Primary);
//	key.addField("a");
//	key.addField("b");
//	test.addKey(key);
//
//	BOOST_CHECK_EQUAL(test.query(), R"(CREATE TABLE "T" ("a" INTEGER PRIMARY KEY AUTOINCREMENT, "b" INTEGER, UNIQUE("a", "b")))");
//}
