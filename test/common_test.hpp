#ifndef TEST_COMMON_TEST_HPP_
#define TEST_COMMON_TEST_HPP_

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <iostream>

#include <boost/test/unit_test.hpp>

#endif // TEST_COMMON_TEST_HPP_
