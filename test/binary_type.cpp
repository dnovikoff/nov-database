#include "common_test.hpp"
#include <nov/database/types/binary.hpp>

using namespace Nov::Database::Types;

static SimpleBinary createZero() {
	return SimpleBinary();
}

static SimpleBinary createBinary(const std::string& x) {
	SimpleBinary sb;
	// Zero byte included
	sb.decode( BinaryData::ref(x.size() + 1, x.data()) );
	return sb;
}

BOOST_AUTO_TEST_CASE ( defaultIsZeroByte ) {
	BOOST_CHECK (createZero().getData().getLength() == 0);
}

BOOST_AUTO_TEST_CASE ( checkData ) {
	const std::string str("Hello!");
	const auto bin = createBinary(str);
	// +zero byte
	BOOST_CHECK_EQUAL(bin.getData().getLength(), str.length()+1);
	// Different addresses
	BOOST_CHECK(bin.getData().as<char>() != str.c_str());

	BOOST_CHECK_EQUAL(bin.getData().toString(), str);
}

static ZipBinary createZipBinary(const std::string& x) {
	ZipBinary sb;
	// Zero byte included
	sb.setData( BinaryData::ref(x.size() + 1, x.data()).copy() );
	return sb;
}

BOOST_AUTO_TEST_CASE ( checkZipData ) {
	const std::string str(
		"This text string should be compressed, because it is long enough to have good compression"
	);
	auto bin = createZipBinary(str);
	const auto binData = bin.getData();

	// String unchanged
	BOOST_CHECK_EQUAL(bin.getData().toString(), str);

	const auto compressed = bin.encode();
	// Compressed should be less than original

	BOOST_REQUIRE(!compressed.isEmpty());

	BOOST_CHECK_LT(compressed.getLength(), binData.getLength());

	// read what is packed
	bin.decode(compressed);
	const auto newBinData = bin.getData();

	BOOST_REQUIRE_EQUAL(binData.getLength(), newBinData.getLength());

	// Data of original and unserialized data matches
	BOOST_CHECK_EQUAL( binData.toString(), newBinData.toString());
	// This is different data
	BOOST_CHECK_NE( binData.as<void>(), newBinData.as<void>());
}

BOOST_AUTO_TEST_CASE ( compareBinariesWithSameData ) {
	BOOST_CHECK_EQUAL(createBinary("Hello"), createBinary("Hello"));
}

BOOST_AUTO_TEST_CASE ( compareBinariesWithDifferentDataOfSameLength ) {
	BOOST_CHECK_NE(createBinary("Hello"), createBinary("Hell1"));
}

BOOST_AUTO_TEST_CASE ( compareDifferent ) {
	BOOST_CHECK_NE(createBinary("Hello"), createBinary("The other binary"));
}

BOOST_AUTO_TEST_CASE ( compareBinSelf ) {
	const SimpleBinary bin = createBinary("Hello");
	BOOST_CHECK_EQUAL(bin, bin);
}

BOOST_AUTO_TEST_CASE ( compareBinToSameAddress ) {
	const SimpleBinary bin = createBinary("Hello");
	const SimpleBinary binCopy = bin;
	BOOST_CHECK_EQUAL(bin, binCopy);
}

BOOST_AUTO_TEST_CASE ( compareZeros ) {
	const SimpleBinary zero1;
	const SimpleBinary zero2;
	BOOST_CHECK_EQUAL(zero1, zero2);
}

BOOST_AUTO_TEST_CASE ( compareBinDataToSameAddress ) {
	const std::string str("Test string");
	BinaryData d1 = BinaryData::ref(str.length(), str.c_str());
	BinaryData d2 = BinaryData::ref(str.length(), str.c_str());
	BOOST_CHECK_EQUAL(d1, d2);
}

BOOST_AUTO_TEST_CASE ( compareSameAddressWithDifferentLength ) {
	const std::string str("Test string");
	BinaryData d1 = BinaryData::ref(str.length(), str.c_str());
	BinaryData d2 = BinaryData::ref(str.length()-1, str.c_str());
	BOOST_CHECK_NE(d1, d2);
}

